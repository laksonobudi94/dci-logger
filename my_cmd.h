


void command_0(void)                                    // 0  read unique identifier
{
    bc_out=22;
    data_out[0]=0xfe;
    data_out[1]=0xE1;
    data_out[2]=un_addr[1];
    data_out[3]=0x05;
    data_out[4]=0x07;
    data_out[5]=0x01;
    data_out[6]=0x01;
    data_out[7]=0x08;
    data_out[8]=0x10;
    data_out[9]=un_addr[2];
    data_out[10]=un_addr[3];
    data_out[11]=un_addr[4];
    data_out[12]=0x05;
    data_out[13]=0x02;
    data_out[14]=0x00;
    data_out[15]=0x02;
    data_out[16]=0x00;
    data_out[17]=0x60;
    data_out[18]=0x56;
    data_out[19]=0x60;
    data_out[20]=0x56;
    data_out[21]=0x01;
}

void command_3(void)                                        //  3  Read Dynamic Variable And Loop Current
{
    bc_out=9;
    q.f=PV_loop_current;

    data_out[0]=q.s[3];
    data_out[1]=q.s[2];
    data_out[2]=q.s[1];
    data_out[3]=q.s[0];

    q.f=PV_metalloss;
    data_out[4]=unit_variable;
    data_out[5]=q.s[3];
    data_out[6]=q.s[2];
    data_out[7]=q.s[1];
    data_out[8]=q.s[0];

}

void command_2(void)                                            // 2 Read loop current and percent of range
{
    bc_out=8;
    q.f=PV_loop_current;

    data_out[0]=q.s[3];
    data_out[1]=q.s[2];
    data_out[2]=q.s[1];
    data_out[3]=q.s[0];

    q.f=PV_percent;
    data_out[4]=q.s[3];
    data_out[5]=q.s[2];
    data_out[6]=q.s[1];
    data_out[7]=q.s[0];

}

void command_6(void)                                                            // 6 write polling address
{
    bc_out=2;

    poll_addr= data_in[0];
    loop_current_mode=data_in[1];


    data_out[0]=poll_addr;
    data_out[1]=loop_current_mode;
    exe=1;
}


void command_40_45_46(void)                         // 40 enter/exit fixed current mode
{                                                   // 45 trim loop current zero
    bc_out=4;                                       // 46 trim loop current gain

    float_hex[3]=data_in[0];
    float_hex[2]=data_in[1];
    float_hex[1]=data_in[2];
    float_hex[0]=data_in[3];
    PV_loop_current= *((float *)&float_hex);



    if(PV_loop_current<4 && PV_loop_current>0 ){
       // response_code=4;
        PV_loop_current=4;
    }
    if(PV_loop_current>20){
       // response_code=3;
        PV_loop_current=20;
    }

    if(PV_loop_current==0 ){
        exe=8;
    }
    else{
        exe=2;
    }
    data_out[0]=float_hex[3];
    data_out[1]=float_hex[2];
    data_out[2]=float_hex[1];
    data_out[3]=float_hex[0];




}

void command_17(void)                                       //17 write message
{
uchar x;
    bc_out=24;

    for(x=0;x<24;x++){
        message[x]=data_in[x];
        data_out[x]=message[x];
    }
    exe=3;

}

void command_18(void)                                       //18 write tag ,descriptor ,  date
{
uchar x;

    bc_out=21;

    for(x=0;x<6;x++){
        tag[x-0]=data_in[x];
        data_out[x]=data_in[x];
    }
    for(x=6;x<18;x++){
        descriptor[x-6]=data_in[x];
        data_out[x]=data_in[x];

    }
    for(x=18;x<21;x++){
        date[x-18]=data_in[x];
        data_out[x]=data_in[x];
    }
    exe=4;
}

void command_49(void)                               // 49 write primary variable transducer S/N
{
uchar x;
    bc_out=3;
    for(x=0;x<3;x++){
        data_out[x]=data_in[x];
        probe_sn[x]=data_in[x];
    }
    exe=5;
}

void command_35(void)                               // 35 write Primary Variable Range
{
uchar x;
    bc_out=9;
    for(x=0;x<9;x++){
        data_out[x]=data_in[x];
    }
 /*
    upper_range[3]=data_in[1];
    upper_range[2]=data_in[2];
    upper_range[4]=data_in[3];
    upper_range[0]=data_in[4];
//    upper_range_value=*(float *)&upper_range;

    lower_range[3]=data_in[5];
    lower_range[2]=data_in[6];
    lower_range[1]=data_in[7];
    lower_range[0]=data_in[8];
//    lower_range_value=*(float *)&lower_range; */
    exe=6;
}

void command_12(void)                               // 12 read Message
{
uchar x;
    bc_out=24;
    for(x=0;x<24;x++){
        data_out[x]=message[x];
    }


}
void command_13(void)                               // 13 read tag ,descriptor ,  date
{
uchar x;
    bc_out=21;
    for(x=0;x<6;x++){
        data_out[x]=tag[x];
    }
    for(x=0;x<12;x++){
        data_out[x+6]=descriptor[x];
    }
    for(x=0;x<3;x++){
        data_out[x+18]=date[x];
    }
}

void command_1(void)                                                // 1 read primary variable
{
    bc_out=5;
    q.f=PV_metalloss;
    data_out[0]=unit_variable;
    data_out[1] =q.s[3];
    data_out[2]=q.s[2];
    data_out[3]=q.s[1];
    data_out[4]=q.s[0];
}

void command_22(void)                                           // 22 write long tag
{
uchar x;
    bc_out=32;
    for(x=0;x<32;x++){
        data_out[x]=data_in[x];
        long_tag[x]=data_in[x];
    }
    exe=7;
}

void command_14(void){                                      //rEAD pRIMARY vARIABLE tRANSDUCER iNFORMATION
uchar x;
    bc_out=16;
    for(x=0;x<16;x++){
        data_out[x]=0;
    }
}

void command_15(void)                                       //Read device information
{
    bc_out=18;

    data_out[0]=250;
    data_out[1]=250;
    data_out[2]=unit_variable;
    q.f=upper_range_value;
    data_out[3]=q.s[3];
    data_out[4]=q.s[2];
    data_out[5]=q.s[1];
    data_out[6]=q.s[0];

    q.f=lower_range_value;
    data_out[7]=q.s[3];
    data_out[8]=q.s[2];
    data_out[9]=q.s[1];
    data_out[10]=q.s[0];

    q.f=1;
    data_out[11]=q.s[0];
    data_out[12]=q.s[1];
    data_out[13]=q.s[2];
    data_out[14]=q.s[3];

    data_out[15]=251;
    data_out[16]=250;
    data_out[17]=0;
}

void command_20(void)                                       //Read long tag
{
uchar x;
    bc_out=32;
    for(x=0;x<32;x++){
        data_out[x]= long_tag[x];
    }
}

void command_48(void){                                      //rEAD additional device status
uchar x;
    bc_out=25;
    for(x=0;x<25;x++){
        data_out[x]=0;
    }
}
void command_34(void){                                      //primary variable damping value
uchar x;
    bc_out=4;
    for(x=0;x<4;x++){
        data_out[x]=data_in[x];
    }
}
void command_16(void){                                      // read final assembly number

    bc_out=3;
    data_out[0]=0;
    data_out[1]=0;
    data_out[2]=32;
}
void command_8(void){                                       //read dynamic variable classification
uchar x;
    bc_out=4;
    for(x=0;x<3;x++){
        data_out[x]=0;
    }
}











void cmd_sel(void)
{
    switch (cmd_in)
    {
        case 3 :
            command_3();
        break;
        case 2 :
            command_2();
        break;
        case 6 :
            command_6();
        break;
        case 40 :
            command_40_45_46();
        break;
        case 45 :
            command_40_45_46();
        break;
        case 46 :
            command_40_45_46();
        break;
        case 17 :
            command_17();
        break;
        case 18 :
            command_18();
        break;
        case 49 :
            command_49();
        case 35 :
            command_35();
        break;
        case 12 :
            command_12();
        break;
        case 13 :
            command_13();
        break;
        case 1 :
            command_1();
        break;
        case 22 :
            command_22();
        break;
        case 15 :
            command_15();
        break;
        case 14 :
            command_14();
        break;
        case 20 :
            command_20();
        break;
        case 48 :
            command_48();
        break;
        case 34 :
            command_34();
        break;
        case 16 :
            command_16();
        break;
        case 8 :
            command_8();
        break;
        };
}



void exe_command(void){
uchar x;
    switch (exe)
    {
        case 1 :

            eeprom1025_write(1, poll_addr);
            eeprom1025_write(2, loop_current_mode);
            //display_id();
            delay_ms(3000);
            menu=0;
            exe=0;
        break;
        case 2  :
            counter_ukur=0;
            set_ad5421(PV_loop_current);
            //display_test_loop();
            exe=20;
        break;
        case 3 :

            for(x=0;x<24;x++){
                eeprom1025_write(3+x,message[x]);
            }
            //glcd_clear();
            //glcd_outtextxyf(0,0,"Message : \n\r");
            //dsp_expand (message,24);
            delay_ms(3000);
            menu=0;
            exe=0;
        break;
        case 4 :

            for(x=0;x<6;x++){
                eeprom1025_write(27+x,tag[x]);
            }
            for(x=6;x<18;x++){
                eeprom1025_write(27+x,descriptor[x-6]);
            }
            for(x=18;x<21;x++){
                eeprom1025_write(27+x,date[x-18]);
            }
            //glcd_clear();
            //glcd_outtextxyf(0,0,"Tag : \n\r");
            //dsp_expand (tag,6);
            //glcd_outtextf("\n\rDescriptor : \n\r");
            //dsp_expand (descriptor,12);
            //glcd_outtextf("\n\rDate : ");
            //num_dsp(date[0],0);glcd_outtextf("-");
            //num_dsp(date[1],0);glcd_outtextf("-");
            //num_dsp(date[2]+1900,0);
            //delay_ms(3000);
            menu=0;
            exe=0;
        break;
        case 5 :

            for(x=0;x<3;x++){
                eeprom1025_write(94+x,probe_sn[x]);
            }
            exe=0;
        break;
        case 6 :

        /*    eeprom1025_write(54,upper_range[0]);
            eeprom1025_write(55,upper_range[1]);
            eeprom1025_write(56,upper_range[2]);
            eeprom1025_write(57,upper_range[3]);


            eeprom1025_write(58,lower_range[0]);
            eeprom1025_write(59,lower_range[1]);
            eeprom1025_write(60,lower_range[2]);
            eeprom1025_write(61,lower_range[3]);

            upper_range_value=*(float *)&upper_range;
            lower_range_value=*(float *)&lower_range;*/
            display_id();
            delay_ms(3000);
            menu=0;
            exe=0;
        break;
        case 7 :

            for(x=0;x<32;x++){
                eeprom1025_write(62+x,long_tag[x]);
            }
            //display_id();
            //delay_ms(3000);
            menu=0;
            exe=0;
        break;
        case 8 :
            exe=0;
            menu=0;
        break;

    };




}