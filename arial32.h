/****************************************************************************
Font created by the LCD Vision V1.18 font & image editor/converter
(C) Copyright 2011-2015 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Font name: Arial Black Bold
The font is proportional (has variable width).
Font height: 36 pixels
First character: 0x20
Last character: 0x7F

Exported font data size:
11872 bytes for displays organized as horizontal rows of bytes
11195 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

#ifndef _ARIAL32_INCLUDED_
#define _ARIAL32_INCLUDED_

extern flash unsigned char arial_32[];

#endif

