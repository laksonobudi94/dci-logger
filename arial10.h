/****************************************************************************
Font created by the LCD Vision V1.18 font & image editor/converter
(C) Copyright 2011-2015 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Font name: Arial Bold
The font is proportional (has variable width).
Font height: 14 pixels
First character: 0x20
Last character: 0x7F

Exported font data size:
1808 bytes for displays organized as horizontal rows of bytes
1486 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

#ifndef _ARIAL10_INCLUDED_
#define _ARIAL10_INCLUDED_

extern flash unsigned char arial10[];

#endif

