/****************************************************************************
Font created by the LCD Vision V1.18 font & image editor/converter
(C) Copyright 2011-2015 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Font name: Lucida Console
Fixed font width: 13 pixels
Font height: 21 pixels
First character: 0x20
Last character: 0x7F

Exported font data size:
4036 bytes for displays organized as horizontal rows of bytes
3748 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

#ifndef _LUCIDA16_INCLUDED_
#define _LUCIDA16_INCLUDED_

extern flash unsigned char lucida_console_16[];

#endif

