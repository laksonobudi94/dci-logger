

#include <io.h>
#include <stdio.h>
#include <math.h>
#include <general.h>
#include <my_Port.h>
#include <my_timer0.h>
#include <my_usart.h>
#include <my_spi.h>
#include <my_i2c.h>
#include <my_lcd.h>



bool sw;
bool no_push;
void main(void)
{
uchar cursor;
uchar cursor_max;
uchar cursor_x;
uchar cursor_y;
uchar cursor_xb;
uchar cursor_yb;
float BE;

float metalloss_temp;
port_init();
delay_ms(1000);
usart_init();
timer0_init ();
spi_init();
my_i2c_init();
my_lcd_init();


#asm("sei")

init_ad5421();
set_ad5421(4);
ads1220_init(0x00,0x00,0xE0,0x00 );
read_reg_ads1220();
probe_type=eeprom1025_read(97);
if (probe_type==255)probe_type=10;
probe_id ();
dsp_menu1();
delay_ms(3000);
menu=0;
key_val=0;

sw=0;


while (1)
      {
      if(sw==1){
        if(key_val==2){
            cursor_xb=cursor_x;
            cursor_yb=cursor_y;
            cursor+=10;
            cursor_y= cursor;
            if(cursor>cursor_max){
                cursor=10;
                cursor_x=0;
                cursor_y=10;
            }

            if(cursor>50){
                cursor_y=cursor-50;
                cursor_x=62;
            }
        }
        if(key_val==3){
            cursor_xb=cursor_x;
            cursor_yb=cursor_y;
            cursor-=10;
            cursor_y= cursor;
            if(cursor<10){
                cursor=cursor_max;
                cursor_y=cursor_max;
                cursor_x=62;
            }
            if(cursor>50){
                cursor_y=cursor-50;
                cursor_x=62;
            }
           if(cursor<60){
                cursor_x=0;
            }
        }


        if(key_val>1 && key_val<10){
            glcd_putcharxy(cursor_xb,cursor_yb,' ');
            glcd_putcharxy(cursor_x+1,cursor_y+1,'>');
            delay_ms(100);
            while(!no_push);
            delay_ms(100);
            key_val=0;
        }
        if(key_val==1){
            if(menu==20){
                if(cursor==10)menu=3;
                if(cursor==20)menu=4;
                if(cursor==30)menu=0;
            }
            if(menu==30){
                if(cursor==10)PV_loop_current=4;
                if(cursor==20)PV_loop_current=8;
                if(cursor==30)PV_loop_current=12;
                if(cursor==40)PV_loop_current=16;
                if(cursor==50)PV_loop_current=20;
                if(cursor==60)menu=2;
                if(cursor<60){
                    PV_metalloss= ((PV_loop_current-4) /16)*thickness;
                    PV_percent=(PV_metalloss/thickness)*100;
                    set_ad5421(PV_loop_current);
                    glcd_setfont(arial8);
                    glcd_clear();
                    glcd_outtextxyf(0,0,"LOOP TEST");
                    glcd_outtextxyf(0,10,"CURRENT :");num_dsp(PV_loop_current,2);
                    glcd_outtextxyf(0,20,"METALLOSS :");num_dsp(PV_metalloss,3);
                    glcd_outtextxyf(0,30,"PERCENT :");num_dsp(PV_percent,1);
                    counter_test_loop=0;
                    key_val=0;
                    while(key_val==0){
                        if(counter_test_loop>4000)key_val=1;
                    }
                    menu=3;
                }
            }
            if(menu==40){
                if(cursor==10)probe_type=10;
                if(cursor==20)probe_type=11;
                if(cursor==30)probe_type=12;
                if(cursor==40)probe_type=20;
                if(cursor==50)probe_type=21;
                if(cursor==60)probe_type=22;
                if(cursor==70)probe_type=23;
                if(cursor==80)probe_type=30;
                if(cursor==90)probe_type=31;
                if(cursor==100)menu=5;
                if(cursor<100){
                    eeprom1025_write(97, probe_type);
                    probe_id();
                    dsp_menu1();
                    delay_ms(4000);
                    menu=0;
                }
            }
            if(menu==50){
                if(cursor==10)probe_type=40;
                if(cursor==20)probe_type=41;
                if(cursor==30)probe_type=50;
                if(cursor==40)probe_type=51;
                if(cursor==50)probe_type=52;
                if(cursor==60)menu=0;

                if(cursor<60){
                    eeprom1025_write(97, probe_type);
                    probe_id();
                    dsp_menu1();
                    delay_ms(4000);
                    menu=0;
                }
            }
            sw=0;
        }


      }
      if(menu==5){
        dsp_menu5();
        menu=50;
        cursor_max=60;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;
        sw=1;
        key_val=4;
      }

      if(menu==4){
        dsp_menu4();
        menu=40;
        cursor_max=100;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;
        sw=1;
        key_val=4;
      }

      if(menu==3){
        dsp_menu3();
        menu=30;
        cursor_max=60;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;
        sw=1;
        key_val=4;
      }

      if(menu==2){
        dsp_menu2();
        menu=20;
        cursor_max=30;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;

        sw=1;
        key_val=4;
      }


      if(menu==0){
        dsp_menu0();
        menu=100;
        counter_ukur=3100;
        key_val=0;
      }
      if(menu==100){
            if(key_val==1){
                menu=2;
            }
            if(counter_ukur>3000){
                cycle_ukur=0;
                metalloss_temp=0;
                gain_set();
                for(cycle_ukur=0;cycle_ukur<10;cycle_ukur++){
                    if(key_val==0){
                        ukur_er();
                        metalloss_temp+=PV_metalloss;
                    }
                    if(key_val==1){
                        menu=2;
                        cycle_ukur=16;
                    }
                }
                if(menu==100){
                    PV_metalloss=metalloss_temp/10;
                    PV_loop_current=((16/(upper_range_value-lower_range_value))*PV_metalloss)+4;
                    PV_percent=(PV_metalloss/(upper_range_value-lower_range_value))*100;
                    set_ad5421(PV_loop_current);
                    glcd_outtextxyf(0,10,"     ");
                    glcd_outtextxyf(0,10,"");
                    glcd_setfont(arial28);
                    num_dsp(PV_metalloss,3);
                    glcd_setfont(arial8);
                    glcd_outtextxyf(50,40,"    ");
                    glcd_outtextxyf(50,40,"");
                    num_dsp(PV_loop_current,2);glcd_outtextf(" mA");
                    glcd_outtextxyf(50,50,"    ");
                    glcd_outtextxyf(50,50,"");
                    num_dsp(PV_percent,1);glcd_outtextf(" %");
                }
                counter_ukur=0;
            }

      }


      }

}





// Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
// Reinitialize Timer 0 value
TCNT0=0x64;
// Place your code here
counter_ukur++;
counter_test_loop++;

if(sw==1){
    counter_reset++;
    if( counter_reset>6000){
        menu=0;
        counter_reset=0;
        sw=0;
    }
}

   if(sw0==0){
        key_val=1;
        counter_reset=0;
        no_push=0 ;
   }
   if(sw1==0){
        key_val=2;
        counter_reset=0;
        no_push=0 ;
   }
   if(sw2==0){
        key_val=3;
        counter_reset=0;
        no_push=0 ;
   }
   if(sw1==1 && sw2==1 && sw0==1){
        no_push=1 ;

   }


}