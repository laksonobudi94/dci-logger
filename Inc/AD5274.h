#ifndef _AD5274_h
#define _AD5274_h

// avr need this
#include <twi_periph.h>
//#include <i2c_peripheral.h>    

#include <delay.h>



/**
 * I2C ADDRESS CONSTANS 
 * I2C addresses of AD5274 depend on ADDR pin connection 
 */

#define ADDRESS_GND 0x2F    /**< Default I2C address iff ADDR=GND, see notes above */
#define ADDRESS_VDD 0x2C    /**< I2C address iff ADDR=VDD, see notes above */
#define ADDRESS_FLOAT 0x2E    /**< I2C address iff ADDR=floating, see notes above */

/**
 * COMMAND CONSTANTS
 * See Table 12, page 21, in the Rev D datasheet
 * Commands are 16-bit writes: bits 15:14 are 0s
 * Bits 13:10 are the command value below
 * Bits 9:0 are data for the command, but not all bits are used with all commands
 */

// The NOP command is included for completeness. It is a valid I2C operation.
#define COMMAND_NOP 0x00    // Do nothing. Why you would want to do this I don't know

// write the 10 or 8 data bits to the RDAC wiper register (it must be unlocked first)
#define RDAC_WRITE 0x01

#define RDAC_READ 0x02    // read the RDAC wiper register

#define TP_WRITE 0x03    // store RDAC setting to 50-TP

// SW reset: refresh RDAC with last 50-TP stored value
// If not 50-TP value, reset to 50% I think???
// data bits are all dont cares
#define RDAC_REFRESH 0x04    // TODO refactor this to AD5274_SOFT_RESET

// read contents of 50-TP in next frame, at location in data bits 5:0,
// see Table 16 page 22 Rev D datasheet
// location 0x0 is reserved, 0x01 is first programmed wiper location, 0x32 is 50th programmed wiper location
#define TP_WIPER_READ 0x05

/**
 * Read contents of last-programmed 50-TP location
 * This is the location used in SW Reset command 4 or on POR
 */
#define TP_LAST_USED 0x06

#define CONTROL_WRITE 0x07// data bits 2:0 are the control bits

#define CONTROL_READ 0x08    // data bits all dont cares

#define SHUTDOWN 0x09    // data bit 0 set = shutdown, cleared = normal mode

/**
 * Control bits are three bits written with command 7
 */
// enable writing to the 50-TP memory by setting this control bit C0
// default is cleared so 50-TP writing is disabled
// only 50 total writes are possible!
#define TP_WRITE_ENABLE 0x01

// enable writing to volatile RADC wiper by setting this control bit C1
// otherwise it is frozen to the value in the 50-TP memory
// default is cleared, can't write to the wiper
#define RDAC_WIPER_WRITE_ENABLE 0x02

// enable high precision calibration by clearing this control bit C2
// set this bit to disable high accuracy mode (dunno why you would want to)
// default is 0 = emabled
#define RDAC_CALIB_DISABLE 0x04

// 50TP memory has been successfully programmed if this bit is set
#define TP_WRITE_SUCCESS 0x08


#define SET_C3 1<<5
#define SET_C2 1<<4
#define SET_C1 1<<3
#define SET_C0 1<<2

#define SET_D0 1<<0
#define SET_D1 1<<1
#define SET_D2 1<<2
#define SET_D3 1<<3
#define SET_D4 1<<4
#define SET_D5 1<<5
#define SET_D6 1<<6
#define SET_D7 1<<7
#define SET_D8 1<<0
#define SET_D9 1<<1

//  Please read:
//  Table 11. Device Address Selection
//  ADDR    A1  A0  7-Bit I2C Device Address
//  GND     1   1   0101111
#define AD5274_addr 0x5E
#define READ_REG    0x01
#define WRITE_REG   0x00
#define RDAC_wiper_register 0x08
#define Software_shutdown   0x24
#define Normal_Mode     0x00
#define CONTENT_REG     0x20    // this register to cek res var
    
void AD527X_Init(uint8_t bytes, uint8_t resistance, uint8_t base); // constructor: base address
uint8_t AD527X_begin(void); // checks that we have connected to the write IC
uint8_t AD527X_command_write(uint8_t command, uint16_t write_datum16);
int16_t AD527X_command_read(uint8_t command, uint8_t data);
int8_t AD527X_control_write_verified(uint8_t control);

void AD527X_enableWiperPosition();
int AD527X_getSetting();
void AD527X_shutdown();


void AD527X_setSettingValue(uint16_t inputValue);  
uint16_t AD527X_getMaximumSetting(); 
void AD527X_increment(); 
void AD527X_decrement(); 
void AD527X_setResistance(uint32_t targetResistance); 
uint32_t AD527X_getResistance(); 
        
#if 0
unsigned char AD5274_wiper_read(void);
void AD5274_wiper_write(unsigned char data);
void AD5274_on(void);
int read_config_reg();
#endif

#endif