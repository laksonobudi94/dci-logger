#ifndef AD7190_H_
#define AD7190_H_

// AVR need this
#include <twi_periph.h>
//#include <i2c_peripheral.h>    

#include <delay.h>
#include <spi.h>
#define adc_cs PORTB.3
#define adc_drdy PINB.2

// hitungan kasar
//#define TIMEOUT_DATA_RDY 1000000        //7 detik
#define TIMEOUT_DATA_RDY 142857         //1 detik
// normalnya hanya nunggu 13.692x

/* Commands to write to specific registers */
#define COM_WRITE_CONFIG_REG_CMD                  0x10
#define COM_WRITE_MODE_REG_CMD                    0x08
#define COM_WRITE_GPCON_RED_CMD                   0x28

/* Commands to read from specific registers */
#define COM_READ_CONFIG_REG_CMD                   0x50
#define COM_READ_STATUS_REG_CMD                   0x40
#define COM_READ_MODE_REG_CMD                     0x48
#define COM_READ_DATA_REG_CMD                     0x58
#define COM_READ_GPCON_REG_CMD                    0x68
#define COM_READ_ID_REG_CMD                       0x60
#define COM_READ_OFFSET_REG_CMD                   0x70
#define COM_READ_FULL_SCALE_REG_CMD               0x78

/* Sampling Rates */
#define FS_100_HZ                                 0x30
#define FS_1200_HZ                                0x04
#define FS_960_HZ                                 0x05
#define FS_2400_HZ                                0x02

/* Register settings commands for Configuration Register */
#define CONFIG_REG_CMD_MSB                        0x08
#define CONFIG_REG_CMD_MID                        0x02

/*May have to change Gain depending on input signal voltage
See Table 19 in AD7190 datasheet for more information*/
#define CONFIG_REG_CMD_LSB                        0x00//0x1F

/* Register settings commands for Mode Register */
#define MODE_REG_CMD_MSB                          0x08
#define MODE_REG_CMD_MID                          0x00
#define MODE_REG_CMD_LSB                          FS_100_HZ

/* Read the data register continously and place the data on DOUT */
#define COMM_REG_CREAD                            0x5C

#define SET_MD0 1<<5
#define SET_CLK1 1<<3
#define SET_SINC3 1<<7
#define SET_FS6 1<<6
#define SET_FS4 1<<4
#define SET_MD2 1<<7
#define SET_CR6 1<<6

#define STATUS_REG      0x00
#define MODE_REG        0x08
#define CONFIG_REG      0x10
#define DATA_REG        0x18
#define ID_REG          0x20
#define GPOCON_REG      0x28
#define OFFSET_REG      0x30
#define FULLSCALE_REG   0x38

#define SUCCESS 0
/* AD7190 API's */
void ADC_Reset();
int ADC_ReadData(float *volt_data);
void ADC_ReadReg(unsigned char reg, unsigned char *reg_data);
void ADC_WriteReg(unsigned char reg, unsigned char *reg_data);
void ADC_WritePort(unsigned char reg_data);
int ADC_InternalCal();

#endif /* AD7190_H_ */