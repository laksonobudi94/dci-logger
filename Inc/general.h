#ifndef _GENERAL_H
#define _GENERAL_H

#define SENDING_DATA_CONFIG     1
#define DOWNLOAD_DATA_HEADER    2
#define DONWLOAD_DATA           3

#define HHU_CONNECTED 0

#define SUCCESS 0

#define uchar unsigned char
#define uint unsigned int
#define ulong unsigned long

uchar type_Logger;

float PV_loop_current;
float PV_percent;
float PV_metalloss;
float PV;

uint data_ke;
uint PV_rcek;

uchar cycle_ukur;
uchar key_val;
uint counter_ukur;
uint counter_test_loop;

uint counter_reset;
uchar menu;
#define hhu_com PIND.5
#define hhu_com2 PIND.4




//float R_sen;
float R_cek;
float BE;
float CE;
float BD;
float RS;
float RC;
float RR;
uchar out_mode;

uchar probe_type;
uchar rumus_ml;
uchar thickness;
float upper_range_value;
float lower_range_value;
float pv_corrate;
uint read_interval;
unsigned int jumlah_data;
char flush_ch[] = "FLUSH ";
char tubular_ch[] = "TUBULAR ";
char tubeloop_ch[] = "TUBELOOP ";
char wireloop_ch[] = "WIRELOOP ";
char sand_ch[] = "SANDPROBE ";

char *probe_ptr;
uchar com_read;
uchar read_now;

//uchar date_time[6]={0};
uchar detik,menit,jam,tanggal,bulan,hdetik;
uchar tahun;

ulong append;

uchar x;
uchar cursor;
uchar cursor_max;
uchar cursor_x;
uchar cursor_y;
uchar cursor_xb;
uchar cursor_yb;
float BE;
void pot_write(uchar pot);
float metalloss_temp;
typedef union
   {
      float          f;
      unsigned char  s[sizeof(float)];
   } v ;
v q;




void num_dsp( float number,uchar koma);


void probe_id (void)
{
    if (probe_type==255)probe_type=10;
    switch (probe_type)
    {  
        // korosion sensor ------------>
        case 10 :
            probe_ptr=flush_ch;
            rumus_ml=1;
            thickness=10;
            upper_range_value=10;
            lower_range_value=0;
        break;
        case 11 :
            probe_ptr=flush_ch;
            rumus_ml=1;
            thickness=20;
            upper_range_value=20;
            lower_range_value=0;
        break;
        case 12 :
            probe_ptr=flush_ch;
            rumus_ml=1;
            thickness=40;
            upper_range_value=40;
            lower_range_value=0;
        break;
        case 20 :
            probe_ptr=tubular_ch;
            rumus_ml=2;
            thickness=10;
            upper_range_value=10;
            lower_range_value=0;
        break;
        case 21 :
            probe_ptr=tubular_ch;
            rumus_ml=2;
            thickness=20;
            upper_range_value=20;
            lower_range_value=0;
        break;
        case 22 :
            probe_ptr=tubular_ch;
            rumus_ml=2;
            thickness=40;
            upper_range_value=40;
            lower_range_value=0;
        break;
        case 23 :
            probe_ptr=tubular_ch;
            rumus_ml=2;
            thickness=80;
            upper_range_value=80;
            lower_range_value=0;
        break;
        case 30 :
            probe_ptr=tubeloop_ch;
            rumus_ml=3;
            thickness=4;
            upper_range_value=4;
            lower_range_value=0;
        break;
        case 31 :
            probe_ptr=tubeloop_ch;
            rumus_ml=3;
            thickness=8;
            upper_range_value=8;
            lower_range_value=0;
        break;
        case 40 :
            probe_ptr=wireloop_ch;
            rumus_ml=4;
            thickness=40;
            upper_range_value=40;
            lower_range_value=0;
        break;
        case 41 :
            probe_ptr=wireloop_ch;
            rumus_ml=4;
            thickness=80;
            upper_range_value=80;
            lower_range_value=0;
        break; 
        // korosion sensor ------------>


        // errosion sensor ------------>
        case 50 :  
            probe_ptr=sand_ch;
            rumus_ml=1;
            thickness=40;
            upper_range_value=40;
            lower_range_value=0;
        break;
        case 51 :
            probe_ptr=sand_ch;
            rumus_ml=1;
            thickness=80;
            upper_range_value=80;
            lower_range_value=0;
        break;
        case 52 :
            probe_ptr=sand_ch;
            rumus_ml=1;
            thickness=120;
            upper_range_value=120;
            lower_range_value=0;
        break;
        // errosion sensor ------------>

    }
}

void output_loop_mode (void)
{
    switch (out_mode)
    {

        case 1 : 
            upper_range_value=10;
            lower_range_value=0;        
        break;          
        case 2 :  
            upper_range_value=100;
            lower_range_value=0;        
        break;          
        case 3 :  
            upper_range_value=1000;
            lower_range_value=0;        
        break;          
    }
}

#endif
