#ifndef _MY_FUNCTION_h
#define _MY_FUNCTION_h

#include <ds1339_twi.h>
#include <24lc1025_twi.h> 

#include <AD5274.h>
#include <stdio.h>

#include <AD7190.h>
#include <ADS1220.h>



/************************************************************************************************
 * Error Diagnostic Message
 ************************************************************************************************/

// EEPROM Fail
#define EEPROM_ERROR     -1    //-->E004

// RTC Controll Reg Fail
// Controll Register Data Not Valid, because bit 6 is 1                    
#define CNTRL_REG_FAIL     -2    //-->E005A
// this 3 data register must be 1
// Oscilator Stop
#define EOSC_FAIL        -3    //-->E005B
// This bit when set to a logic 1 enables the square wave
// or interrupt output when VCC is absent and the DS1339 is being powered by the VBACKUP pin
// harusnya 1 tapi disini 0
#define BBSQI_FAIL        -4    //-->E005C
// This bit controls the relationship between the two alarms and the interrupt output
// pins. When the INTCN bit is set to logic 1, a match between the timekeeping registers and the alarm 1 or alarm 2
// registers activate the SQW/INT pin (provided that the alarm is enabled).
// harusnya 1 tapi disini 0                    
#define INTCN_FAIL        -5    //-->E005D

// Set Alarm Failed
// out of range
#define GET_TIME_FOR_ALARM_FAIL     -6    //-->E005E
#define GET_ALRM_FAIL    -7        //-->E005F

// Ukur ER Failed
//drdy ADC not show
#define DRDY_ADC_FAIL         -8    //-->E007A
//register config not same
#define CONF_REG_ADC_FAIL 	-9	//-->E007B

//probe error
// Rsen
#define BC_READ_FAIL 		-10 //-->E1
// R check
#define DC_READ_FAIL 		-11 //-->E2
#define EC_READ_FAIL 		-12 //-->E3

// Time Stamp Failed
// out of range
#define GET_TIME_FOR_STAMP_FAIL 		-13	//-->E005G
#define GET_DATE_FOR_STAMP_FAIL 		-14	//-->E005H

// failed 10 time akuisi data
#define TEN_TIME_FAIL 			-15	//-->E10

// wiper config incorrect
#define WIPER_CONF_FAIL 			-16	//-->E006
                                
#define SENSORING_NOT_RECOGNIZE         -17    //-->E003

// batasin segini
#define MAX_STORED_DATA 4000

#define SAVE_TIME_STAMP(val)    rtc_to_mem(201+(val*11))

#define SUCCESS 0
    
struct tm{
    // time
    unsigned char hour;
    unsigned char minute;
    unsigned char second;
    // date
    unsigned char day;
    unsigned char month;
    unsigned char year;
};

struct data_save
{
       unsigned char mnt;
       unsigned char jm;
       unsigned char tgl;
       unsigned char bln;
       unsigned char thn;
       float ML_CR;
       unsigned int cek_temp;
};

struct data_header
{
       unsigned char prb_typ;
       char tages[15];
       unsigned int rd_intrvl;
       unsigned char mnt;
       unsigned char jm;
       unsigned char tgl;
       unsigned char bln;
       unsigned char thn;
};

float data_to_current_value(unsigned char data);
unsigned char current_to_data_value(float *arus);
int cek_controll_reg_ds(unsigned char data);
int cek_status_reg_ds(unsigned char data);
int set_alarm(unsigned int rentang_menit);
int rtc_to_mem(unsigned long addr);
struct tm read_time_mem(long addr);
void pot_write(unsigned char pot);
int i2c_scanner(void);
void dump_data(unsigned char *cek_data,unsigned long addr_data);
unsigned int nyari_read_interval(void);

uint8_t write_data_config_to_eeprom(uint16_t addr,unsigned char val);
uint8_t read_total_data_from_eeprom(unsigned int *total);    
uint8_t write_total_data_to_eeprom(unsigned int val);
uint8_t read_metalloss_from_eeprom(float *ml,unsigned int offset);
uint8_t write_metalloss_to_eeprom(float ml,unsigned int offset);
uint8_t read_byte_calculation(unsigned int offset,unsigned char bite,unsigned char *value);
uint8_t write_pv_rcek_to_eeprom(unsigned int offset,unsigned int res);
uint8_t read_corrate_from_eeprom(float *value);
uint8_t write_corrate_to_eeprom(float value);
uint8_t read_probe_type(unsigned char *tipe);

#endif
