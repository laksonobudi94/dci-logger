#ifndef _DS1399_h
#define _DS1399_h

#include <bcd.h>    // bcd_to_bin function

// AVR need this
//#include <i2c.h>  
#include <twi_periph.h>    
  
#include <delay.h>  

// Please Read:
// Table 3. Timekeeper Registers 
#define ADDR_DS1399                 0xD0
#define ADDR_SECONDS                0x00
#define ADDR_ALARM1_SECONDS         0x07
#define ADDR_ALARM2_MINUTES         0x0B
#define ADDR_DAY                    0x04
#define ADDR_STATUS                 0x0F

#define READ_CMD                    0x01
#define WRITE_CMD                   0x00

#define SET_A1M4                    0x80
#define SET_A2M4                    0x80

#define CONTROL_REGISTER            0x0E
#define STATUS_REGISTER             0x0F

#define SET_BBSQI                   0x20
#define SET_INTCN                   0x04
#define SET_A2IE                    0x02
#define SET_A1IE                    0x01

#define TRICKLE_CHARGER_REGISTER    0x10

#define OSF_BIT                     1<<7

int ds1339_get_time(unsigned char *hour,unsigned char *min,unsigned char *sec);
int ds1339_get_date(unsigned char *day,unsigned char *month,unsigned char *year);
void ds1339_set_time(unsigned char hour,unsigned char min,unsigned char sec);
void ds1339_set_date(unsigned char day,unsigned char month,unsigned char year);
void ds1339_set_alrm1(unsigned char hour,unsigned char min,unsigned char sec);
int ds1339_get_alrm1(unsigned char *hour,unsigned char *min,unsigned char *sec);
void ds1339_set_alrm2(unsigned char hour,unsigned char min);
int ds1339_get_alrm2(unsigned char *hour,unsigned char *min);
int ds1339_get_status(unsigned char *statE,unsigned char *statF);
void ds1339_init(void);
void ds1339_alarm1_on(void);
void ds1339_alarm2_on(void);


#endif // !_DS1399_h