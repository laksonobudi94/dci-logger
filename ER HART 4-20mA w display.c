#include <io.h>
#include <stdio.h>
#include <math.h>
#include <general.h>
#include <my_Port.h>
#include <my_timer0.h>
#include <my_usart.h>
#include <my_spi.h>
#include <my_i2c.h>
#include <my_lcd.h>



bool sw;
bool no_push;
uchar reply;



void main(void)
{
uchar x;
uchar cursor;
uchar cursor_max;
uchar cursor_x;
uchar cursor_y;
uchar cursor_xb;
uchar cursor_yb;
float BE;

float metalloss_temp;


port_init();
delay_ms(1000);
usart_init();
timer0_init ();
spi_init();
my_i2c_init();
my_lcd_init();


#asm("sei")






    init_ad5421();
    ads1220_init(0x00,0x00,0xE0,0x00 );
    probe_type=eeprom1025_read(97);
    if (probe_type==255)probe_type=10;
    menu=0;
    key_val=0;
    sw=0;



while (1)
      {


    
      if(sw==1){
        if(key_val==2){
            cursor_xb=cursor_x;
            cursor_yb=cursor_y;
            cursor+=10;
            cursor_y= cursor;
            if(cursor>cursor_max){
                cursor=10;
                cursor_x=0;
                cursor_y=10;
            }

            if(cursor>50){
                cursor_y=cursor-50;
                cursor_x=62;
            }
        }
        if(key_val==3){
            cursor_xb=cursor_x;
            cursor_yb=cursor_y;
            cursor-=10;
            cursor_y= cursor;
            if(cursor<10){
                cursor=cursor_max;
                cursor_y=cursor_max;
                cursor_x=62;
            }
            if(cursor>50){
                cursor_y=cursor-50;
                cursor_x=62;
            }
           if(cursor<60){
                cursor_x=0;
            }
        }


        if(key_val>1 && key_val<10){
            glcd_putcharxy(cursor_xb,cursor_yb,' ');
            glcd_putcharxy(cursor_x+1,cursor_y+1,'>');
            delay_ms(100);
            while(!no_push);
            delay_ms(100);
            key_val=0;
        }
        if(key_val==1){
            if(menu==20){
                if(cursor==10)menu=3;
                if(cursor==20)menu=4;
                if(cursor==30)menu=0;
            }
            if(menu==30){
                if(cursor==10)PV_loop_current=4;
                if(cursor==20)PV_loop_current=8;
                if(cursor==30)PV_loop_current=12;
                if(cursor==40)PV_loop_current=16;
                if(cursor==50)PV_loop_current=20;
                if(cursor==60)menu=2;
                if(cursor<60){
                    set_ad5421(PV_loop_current);
                    display_test_loop();
                    counter_test_loop=0;
                    key_val=0;
                    while(key_val==0){
                        if(counter_test_loop>4000)key_val=1;
                    }
                    menu=3;
                }
            }
            if(menu==40){
                if(cursor==10)probe_type=10;
                if(cursor==20)probe_type=11;
                if(cursor==30)probe_type=12;
                if(cursor==40)probe_type=20;
                if(cursor==50)menu=0;
                if(cursor<100){
                    eeprom1025_write(97, probe_type);
                    probe_id();
                    delay_ms(4000);
                    menu=0;
                }
            }
           
            sw=0;
        }


      }


      if(menu==4){
        dsp_menu4();
        menu=40;
        cursor_max=50;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;
        sw=1;
        key_val=4;
      }

      if(menu==3){
        dsp_menu3();
        menu=30;
        cursor_max=60;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;
        sw=1;
        key_val=4;
      }

      if(menu==2){
        dsp_menu2();
        menu=20;
        cursor_max=30;
        cursor=10;
        cursor_x=0;
        cursor_y=cursor;
        cursor_xb=62;
        cursor_yb=cursor_max;

        sw=1;
        key_val=4;
      }


      if(menu==0){
        dsp_menu0();
        menu=100;
      }
      if(menu==100){
            if(key_val==1){
                menu=2;
            }
      }


      }
    


}





// Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
// Reinitialize Timer 0 value
TCNT0=0x64;
// Place your code here
counter_ukur++;
counter_test_loop++;

if(sw==1){
    counter_reset++;
    if( counter_reset>6000){
        menu=0;
        counter_reset=0;
        sw=0;
    }
}

   if(sw0==0){
        key_val=1;
        counter_reset=0;
        no_push=0 ;
   }
   if(sw1==0){
        key_val=2;
        counter_reset=0;
        no_push=0 ;
   }
   if(sw2==0){
        key_val=3;
        counter_reset=0;
        no_push=0 ;
   }
   if(sw1==1 && sw2==1 && sw0==1){
        no_push=1 ;

   }






}