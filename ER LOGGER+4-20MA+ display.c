//#define SENSORING_NOT_USED
//#define USE_USB                 
#define DEBUG_DCI

#include <io.h>
#include <stdio.h>
#include <string.h> 
#include <math.h>
#include <bcd.h>
#include <my_Port.h>
// #include <twi.h>    
//#include <i2c.h>
#include <twi_periph.h>

//#include <i2c_peripheral.h>    

#include <general.h>
#include <stdint.h>
#include <eeprom.h>
//#include <pcf8583.h>

#include <my_usart.h>

#include <my_spi.h>
#include <ds1339_twi.h>
//#include <my_i2c.h>   
#include <24lc1025_twi.h>   

#include <my_lcd.h>
#include <my_timer0.h>
#include <my_usart.h>

#include <ukur_ER_sen_coak.h>
#include <ukur_LPR.h>
  

#include "my_function.h"
struct tm waktu;

void WDT_off(void);
void WDT_on(void);
void calculate_corrate(void);

unsigned char zeroing_val = 0;

unsigned char respon;
unsigned char buffer_in[40];
unsigned char buffer_out[40];
unsigned char byte_count;
unsigned char command_in;

extern float ML_atas;

unsigned char read;


uchar reply;
uint int_;
unsigned char intrpt;
// Watchdog timeout interrupt service routine
interrupt [WDT] void wdt_timeout_isr(void)
{
    // Place your code here
    glcd_outtextxyf(100,0,"INT");   
    
    EERead(60000,&intrpt,1);
    int_ = intrpt;              
    
    int_++; 
    intrpt = int_;                                  
    EEWrite(60001,&intrpt,1);   
    
    //  glcd_outtextxyf(0,50,"");num_dsp(int_,0); 
    
    delay_ms(1000);
    
    //  set_alarm(2);
    //  rtc_write( 0, 0x04  );    
    //  rtc_write( 8, 0x90  );
}


uint8_t bootKey = 0x77;
uint8_t *bootKeyPtr = (uint8_t *)0x0800;

uint8_t cek_signature(void){
    //  printf("byt: %02X %02X\r\n",eeprom_read_byte(0x1D),eeprom_read_byte(0x1E));

    if(eeprom_read_byte(0x1D)==0x12 && eeprom_read_byte(0x1E)==0x34){
        return 1;
    }else
        return 0;
}

int test_rw_eeprom(void);

// blinked 1 minutes ERROR message diagnostic
void show_error_diagnostic(int value_err){
    unsigned char blinked;
             
    glcd_printfxy(0,50,"          ");   
    //printf("blinked 1 minute\r\n");
    for(blinked=0;blinked<30;blinked++){
        switch(value_err){
            case EEPROM_ERROR:
             glcd_printfxy(0,50,"E004"); break;   
                    
            case CNTRL_REG_FAIL:
                glcd_printfxy(0,50,"E005A"); break;   
            case EOSC_FAIL:
                glcd_printfxy(0,50,"E005B"); break;         
            case BBSQI_FAIL:
                glcd_printfxy(0,50,"E005C"); break; 
            case INTCN_FAIL:
                glcd_printfxy(0,50,"E005D"); break; 
            case GET_TIME_FOR_ALARM_FAIL:
                glcd_printfxy(0,50,"E005E"); break; 
            case GET_ALRM_FAIL:
                glcd_printfxy(0,50,"E005F"); break; 
                        
            case DRDY_ADC_FAIL:
                glcd_printfxy(0,50,"E007A"); break;     
            case CONF_REG_ADC_FAIL:
                glcd_printfxy(0,50,"E007B"); break; 

            case BC_READ_FAIL:
                glcd_printfxy(0,50,"E1"); break; 
            case DC_READ_FAIL:
                glcd_printfxy(0,50,"E2"); break; 
            case EC_READ_FAIL:
                glcd_printfxy(0,50,"E3"); break; 

            case GET_TIME_FOR_STAMP_FAIL:
                glcd_printfxy(0,50,"E005G"); break; 
            case GET_DATE_FOR_STAMP_FAIL:
                glcd_printfxy(0,50,"E005H"); break;
                
            case WIPER_CONF_FAIL:
                glcd_printfxy(0,50,"E006"); break;
                                         
            case TEN_TIME_FAIL:
                glcd_printfxy(0,50,"E10"); break;
                
            case SENSORING_NOT_RECOGNIZE:
                glcd_printfxy(0,50,"E003"); break;
             
                                         
            default: break;                 
        }    
        //printf("delay Error\r\n");

        delay_ms(1500);
        glcd_printfxy(0,50,"             "); 
        delay_ms(500);
    }

}

void main(void)
{

    uchar e;
    uchar m;
    uchar n;
    uint k;
    uchar x;          
    float lpr_cor_rate;  
        
    uchar value; 
    #define ER_LOGGER 0
    #define LPR_LOGGER 1

    


    struct value_ukur nilai_ukur;
    struct data_save data_send;
                
    
    // LAKSONO: debug RTC
    uchar test_detik,test_menit,test_jam,test_tanggal,test_bulan,test_tahun;
    unsigned char start_sekian_kali;

    unsigned char Status_Reg;
    unsigned char Controll_Reg;
    unsigned char Cek_Controll_Reg;  
    
    int report_sens;
    int status_diagnostic;
    int diagnostic_err;   
    unsigned int try_x;               
    unsigned long alamat_eep;   
    
    dsp_error_msg=0; 
    menu_error_msg=0; 
                             
                                       

    START:    
    //printf("menu_error_msg init %d\r\n",menu_error_msg);

    EERead(60002,&dsp_error_msg,1) ;   
    
    // jika eeprom ga kebaca force 1
    if(dsp_error_msg>1)dsp_error_msg=1;              
    if(status_diagnostic == EEPROM_ERROR)dsp_error_msg=1;
    
    // JIKA JUMP KE START 3X   
    #if 1        
    if(dsp_error_msg==1){
        if(start_sekian_kali>3){
         
           printf("Error 3 x\r\n");
     
           start_sekian_kali = 0;  
           
           if (hhu_com != HHU_CONNECTED){
                // blink Error Message in LCD 1 minutes
                show_error_diagnostic(status_diagnostic);  
                // ketika 10x pembacaan ga sesuai range R_Check & batas atas - bawah
                // simpan apa ada nya
                if(status_diagnostic == TEN_TIME_FAIL)goto SIMPAN;
           }
           
           // pada saat EEPROM tak terpasang force interval 1 menit 
           if(status_diagnostic == EEPROM_ERROR)read_interval=1;
           else
               read_interval = nyari_read_interval();
         
           set_alarm(read_interval);
                        
           // wait alarm int
           ds1339_alarm1_on();
        }    
        #endif
        start_sekian_kali++;  
    }        
                   

    MCUSR = 0; // clear reset flags
    WDTCSR = (1<<WDCE) | (1<<WDE); // enable change
    WDTCSR = 0; // watchdog off


    port_init();

    /*
    if(cek_signature()!=1){
        *bootKeyPtr = bootKey;
            		
        WDTCSR = (1<<WDCE);  // Watchdog Change Enable
        WDTCSR = (1<<WDE);  // Enable system reset and set clock pre-scale mode to 0 == 2048 counts == about 16 mS.
        #asm("sei")
        while (1);
    }
    */

    timer0_init ();
    usart_init();

    //spi_init();
    i2c_periph_init();

    my_lcd_init();
    WDT_on();
    #asm("sei")
                
                        
    // Cek Status Untuk Indikasi menggunakan sensoring LPR kah?
    // jika tidak terdeteksi bisa jadi rusak atau pakai sensoring ER.
    if(LMP91000_status() == LMP91000_READY)  {
        type_Logger = LPR_LOGGER;
        printf("This is LPR_LOGGER...;");  
    }   
    else{                                     

        spi_enable();  
        ADC_Reset();
        ADC_ReadReg(ID_REG,&value);   

        //printf("ID_REG 0x%x\r\n",value);

        if(value & 0x0F == 0x04){
            type_Logger = ER_LOGGER;
            printf("This is ER_LOGGER...;");  
        }        
        else{
            status_diagnostic = SENSORING_NOT_RECOGNIZE;
            printf("Not Recognize Sensoring...;");  
            goto START;        
        }         
    }                            
        
    delay_ms(5);
    read_now=0;
    read=0;
    menu=2;
    key_val=0;
    sw=0;   
    dsp_alarm=0; 
    respon=0;

    command_in=0;     
                         

    if(menu_error_msg==0){
        glcd_outtextxyf(5,25,"KSHHU CONNECTED");     
        //glcd_outtextxyf(0,50,"S/N : 0123456789");         
    }

while (1)
    {               
    
     /* 
        // LAKSONO: debug RTC
        ds1339_get_time(&test_jam,&test_menit,&test_detik);  
        ds1339_get_date(&test_tanggal,&test_bulan,&test_tahun);         
        printf("Date : %d-%d-%d Time : %d:%d:%d\n\r",test_tanggal,test_bulan,test_tahun,test_jam,test_menit,test_detik);
       */  
                    
    if (hhu_com != HHU_CONNECTED)
    {          
        glcd_clear();
   
        /*
            // glcd_outtextxyf(0,0,"test");
                            
                    
            jumlah_data=(eeprom1025_read(25)*256) + eeprom1025_read(26); 
            // delay_ms(500);
            rtc_to_mem(201+(jumlah_data*11)); 
            // printf("%i. %d/%d/%d %d:%d   \n\r ",jumlah_data,tanggal,bulan,tahun,jam,menit);                         
            nyari_read_interval();

            set_alarm(read_interval);    
            read=0;   
            probe_type=eeprom1025_read(0);                      
            probe_id();     
            // calculate_corrate();             
            counter_ukur=0;        
            jumlah_data=(eeprom1025_read(25)*256) + eeprom1025_read(26);        
            data_ke=jumlah_data+1;
            eeprom1025_write(25,data_ke>>8);
            eeprom1025_write(26,data_ke);    
            jumlah_data=jumlah_data%4000;
            read=1;            
        */        
                                                              
        if(dsp_error_msg==1){
            //printf("diagnos ee\r\n");  
            status_diagnostic = test_rw_eeprom();
            //printf("diagnos ee done %d\r\n",status_diagnostic);  
            if(status_diagnostic != SUCCESS){     
                // EEPROM Write Read Failed   
                goto START;
            }                    
        }     

        glcd_outtextxyf(110,0,"0"); // debug EEPROM fail read
                         
        if(read_total_data_from_eeprom(&jumlah_data) != 0){
            if(dsp_error_msg==1){
                status_diagnostic = EEPROM_ERROR;
                goto START;
            }
        }
                                
        
        glcd_outtextxyf(110,0,"0.5");  // debug EEPROM success read           
        data_ke=jumlah_data+1;  
                                               
        // max 4000 data
        // jika > 4000 replace mulai dari 0        
        jumlah_data=jumlah_data % MAX_STORED_DATA;   
        
        if(read_metalloss_from_eeprom(&PV_metalloss,jumlah_data) != 0){
            if(dsp_error_msg==1){
                status_diagnostic = EEPROM_ERROR;
                goto START;
            }
        }    
        printf("[Metalloss Old] %f\n\r",PV_metalloss);
                 
        
        // pastikan PV_metalloss di titik awal itu 0
        ML_atas  =  PV_metalloss;
        
        if (jumlah_data == 0)PV_metalloss=0;  
    
        dsp_menu0(type_Logger);   // display last value
        if(data_ke>0)
            glcd_printfxy(0,50,"%d",data_ke-1);            

    #if 1                                            
        if(ds1339_get_status(&Controll_Reg,&Status_Reg)!=0){
            if(dsp_error_msg==1){
                 status_diagnostic = CNTRL_REG_FAIL;            
                 goto START;
            }
        }
        
        printf(">>> Status_Reg RTC %d\n\r",Status_Reg);                          
        printf(">>> Controll_Reg RTC %d\n\r",Controll_Reg);                          
          
        #ifndef USE_USB                                
            status_diagnostic = cek_controll_reg_ds(Controll_Reg);
            
            if(dsp_error_msg==1){
                if(status_diagnostic != SUCCESS){
                    printf(">>> status_rtc %d\n\r",status_diagnostic);                          
                    goto START;
                }
            }         
        #endif
   
        read_interval = nyari_read_interval();
        printf(">>> Read_Interval = %d \n\r",read_interval);                          
        status_diagnostic = set_alarm(read_interval);
        if(dsp_error_msg==1){
            if(status_diagnostic != SUCCESS){
                // RTC Failure  
                printf(">>> RTC Failure\n\r");                          
                goto START;
            }
        }          
                            
        if(read_probe_type(&probe_type) != 0){
            if(dsp_error_msg==1){
                status_diagnostic = EEPROM_ERROR;
                goto START;
            }
        }    
                          
        probe_id();
        printf(">>> probe_type %d\n\r",probe_type);                          
                       
        glcd_outtextxyf(110,0,"1"); // debug EEPROM R/W Success                     
        WDT_off();

        glcd_outtextxyf(110,0,"2"); // debug Watchdog Off Success                    
                                                                   
        //#ifndef SENSORING_NOT_USED
        // ignore ADC Data for testing
        #if 1
        if(type_Logger == LPR_LOGGER){
            ukur_lpr(&lpr_cor_rate);        
            
            if (alarm >0)   // kalo ada alarm masuk sini, alarm taro di Rcek buat info aja
            {
                 glcd_printfxy(80,54,"E %d",alarm);
                 R_cek=alarm;
                 PV_rcek=R_cek;      
                 delay_ms(10000) ; 
                        
            }else{
                 
                 R_cek=0; 
                 PV_rcek=R_cek*1000;        

            }
        }
        else if(type_Logger == ER_LOGGER){
            // if fail metalloss will be 0
            status_diagnostic = ukur_er(&nilai_ukur,rumus_ml,thickness,probe_type);
            if(status_diagnostic != SUCCESS){
                printf("report_sens %d \n\r",status_diagnostic);                              
                goto START;
            }            
        }    
        #else
            printf("Sensoring Not Used\n\r");                              
        #endif  

SIMPAN:                                       
        status_diagnostic = SAVE_TIME_STAMP(jumlah_data);
        if(dsp_error_msg==1){
            if(status_diagnostic!=SUCCESS){    
                // rtc value invalid
                goto START;         
            }       
        }
                
        WDT_on();
                                 
        if(type_Logger == ER_LOGGER){
            PV_metalloss = nilai_ukur.PV_Metal_Loss;
            R_cek = nilai_ukur.R_Check;

            PV_rcek = R_cek*1000;  
        }
        else if(type_Logger == LPR_LOGGER){ 
            PV_metalloss = lpr_cor_rate;
        }
        
        
        
        /*
            glcd_outtextxyf(110,0,"3");               
              
            PV_metalloss=0.2;
            PV_rcek=295;
            printf("%1.3f  %d\n\r",  R_sen,PV_rcek);
        */                  
                      
        printf("[Metalloss New] %f\n\r",PV_metalloss);
                     
        if(write_metalloss_to_eeprom(PV_metalloss,jumlah_data) != 0){
            if(dsp_error_msg==1){
                status_diagnostic = EEPROM_ERROR;
                goto START;
            }
        }
          
        if(write_pv_rcek_to_eeprom(jumlah_data,PV_rcek) != 0){
            if(dsp_error_msg==1){
                status_diagnostic = EEPROM_ERROR;
                goto START;
            }
        }
        
        // LAKSONO:
        // BUG: Data Problem
        // sebelumnya ini ditulis di awal setelah baca jumlah data langsung di replace
        // harusnya kan akuisi data dulu bener baru di tambahin dong    
        // replace jumlah data dengan data_ke
        if(write_total_data_to_eeprom(data_ke) != 0){
            if(dsp_error_msg==1){
                status_diagnostic = EEPROM_ERROR;
                goto START;
            }
        }
          
        dsp_menu0(type_Logger);    // display new value   
        
        // delay_ms(3000) ;                         
        glcd_outtextxyf(110,0,"3");     

        glcd_printfxy(0,50,"%d",data_ke);            
                        
        status_diagnostic = 0;
        
        read = 0;   
        ds1339_alarm1_on();
        read_now = 1;    
                              
        // LAKSONO
        // debug ALARM
        // printf("Display ALARM...\n\r");
        // glcd_printfxy(0,40,"TIME : %d:%d:%d",test_jam,test_menit,test_detik);
        // ds1339_get_alrm1(&test_jam,&test_menit,&test_detik);                            
        // glcd_printfxy(0,50,"ALRM : %d %d %d",test_jam,test_menit,test_detik);
                            
    #endif 
    
        delay_ms(2000);
    }
         
          
    else
    {        
        /*
          Untuk Logger DCI-DX hhu_com harus di Set 0
         * dari KSHHU
         */ 
        // printf("hhu_com==0\n\r");             
               
        // untuk DCI Logger Setting di lakukan dari KSHHU
        // menu_exe(); 

        // glcd_printfxy(0,0,"% 3u",ptr);

        if (dsp_alarm==1)
        {


                
        }

        if (respon==1)
        {                       
            respon=0;        
            
            //delay_ms(100);          
                         
            // cek message    
            if (command_in == SENDING_DATA_CONFIG)
            {         
                // write con   
                for (e=1;e<=byte_count;e++)
                {               
                    write_data_config_to_eeprom(e-1, buffer_in[e]);
                }            
                     
                // pv_corrate set to 0
                write_corrate_to_eeprom(0);    
                
                probe_type=buffer_in[1];    
                                    
                // read time config from KS HHU
                waktu = read_time_mem(20);  
                jam = waktu.hour;
                menit = waktu.minute;
                
                ds1339_set_time(jam,menit,0);    
                
                // write 2 byte
                write_total_data_to_eeprom(0);
                
                tanggal = waktu.day;
                bulan = waktu.month;
                tahun = waktu.year;
                ds1339_set_date(tanggal,bulan,tahun);
                            
                send_command(REPLY_DATA_CONFIG,1,probe_type);                                   

                PV_metalloss = 0;
                pv_corrate = 0;      

                ds1339_init();
                // wait 2 second in first alarm
                ds1339_set_alrm1(jam,menit,detik+2);  
                ds1339_alarm1_on();             
                 
                // debug ALARM
                // printf("display ALARM---->>\n\r");
                // ds1339_get_alrm1(&test_jam,&test_menit,&test_detik);                            
                // glcd_printfxy(0,50,"ALRM : %d %d %d",test_jam,test_menit,test_detik);
                               
                //glcd_printfxy(0,50,"CONFIGURED");
                delay_ms(2000);                
            }
            if (command_in == DOWNLOAD_DATA_HEADER)                     
            {
                for (e=0;e<27;e++)
                {
                    EERead(e,(uint8_t *)&buffer_out[e],1);                                        
                }    
                append = 0;                  
                read_total_data_from_eeprom(&jumlah_data);      
                if (jumlah_data > MAX_STORED_DATA)append= jumlah_data % MAX_STORED_DATA;                                            
                                             
                send_command(REPLY_DOWNLOAD_HEADER,27,probe_type);              
                
                //glcd_printfxy(0,50,"READ CONFIG");
            }            
            if (command_in==DONWLOAD_DATA)
            {   
                      
                data_ke=(append+(buffer_in[1]*256)+buffer_in[2])%MAX_STORED_DATA;                   
                                                                                                                                              
                //  eeprom1025_read_array(buffer_out,201+(data_ke*11),11);                
                 
                try_x=0;     
COBA_LAGI:
                try_x++;     
                for (e=0;e<11;e++)
                {                         
                    read_byte_calculation(data_ke,e,&buffer_out[e]);                                     
                } 
                                       
                //alamat_eep = 201+(data_ke*11);
                                     
                memcpy(&data_send,&buffer_out[0],sizeof(data_send));
                if(bcd2bin(data_send.mnt) < 0 || bcd2bin(data_send.mnt) >59 ||
                    bcd2bin(data_send.jm) < 0 || bcd2bin(data_send.jm) > 23 ||
                    bcd2bin(data_send.tgl) < 0 || bcd2bin(data_send.tgl) > 31 ||
                    bcd2bin(data_send.bln) < 0 || bcd2bin(data_send.bln) > 11 ||
                    data_send.thn < 0 || data_send.thn > 99                
                ) {                  
                    //glcd_printfxy(0,50,"Trying (%d)",try_x);                               
                    //glcd_printfxy(0,50,"                                 ");
                    goto COBA_LAGI;
                }                                                           
                
                send_command(REPLY_DOWNLOAD_DATA,11,probe_type);                            
                //glcd_printfxy(0,50,"DOWNLOADING");

            }            
            command_in=0;
            respon=0; 
        } 
    }
  
    //delay_ms(10);
 }
     
}


void calculate_corrate(void)
{
    float u;
    float k;
    float w;
    float xi;
    float ey;
    float e2;
    uint i;
    uint j;
    uchar m;

    // glcd_setfont(arial8);

    append=0; 
    read_total_data_from_eeprom(&jumlah_data);   
     
    read_metalloss_from_eeprom(&PV_metalloss,jumlah_data);
         
    if (jumlah_data>MAX_STORED_DATA){
        append= jumlah_data%MAX_STORED_DATA;
        read_metalloss_from_eeprom(&PV_metalloss,append);    
        
    }        
    // jika baru pertama kali ambil data maka nilai metalloss force to 0
    if (jumlah_data==0)PV_metalloss=0;
    if (jumlah_data ==65535)
       {
        jumlah_data=0;
       }       
    read_corrate_from_eeprom(&pv_corrate);

    dsp_menu0(type_Logger);

    // glcd_setfont(arial8);
    // glcd_outtextxyf(110,0,"D1"); 
    
    read_interval = nyari_read_interval();

    // glcd_setfont(arial8);
    // glcd_outtextxyf(110,0,"D2");

    // interval in minute
    xi=read_interval;  
    // interval in hour
    xi=xi/60;
       
    // glcd_outtextxyf(0,10,"read_interval = ");num_dsp(read_interval,0); 

    j = jumlah_data;
    if (jumlah_data > MAX_STORED_DATA)j = MAX_STORED_DATA;   

    // calculate E Xi    
    k=0-xi; 
    w=0;        
    for (i=0;i<j;i++)
    {           
        k=k+(xi);   
        w=w+k;  
    }                 
               
    // u = E Xi / m
    // E Xi (sigma Xi) = sum of time base value in hour
    // m = the number of points used for regression formula          
    u=w/j;
               
    // glcd_outtextxyf(110,0,"D3");
    
    ey=0;    
    k=0-xi; 

    // glcd_outtextxyf(0,0,"");num_dsp(jumlah_data,0);    
    // delay_ms(1000);      
  
    e2=0;        

    WDT_off();
                       
    for (i=append;i<append+jumlah_data;i++)
    {        
        // delay_ms(1);
        // q.s[3]=eeprom1025_read(201+(i*11)+5);
        // q.s[2]=eeprom1025_read(201+(i*11)+6);
        // q.s[1]=eeprom1025_read(201+(i*11)+7);
        // q.s[0]=eeprom1025_read(201+(i*11)+8);
        // PV_metalloss=q.f;
                    
        read_metalloss_from_eeprom(&PV_metalloss,i);   
                            
        k=k+(xi);    
        // E yi (xi - �) 
        ey=ey+(PV_metalloss*(k-u));     
        // E (xi - �)2
        e2=e2+((k-u)*(k-u));                  
    }          
    
    // glcd_outtextxyf(110,0,"D4");    
 
    for (i=0;i<append;i++)
    {                              
        // delay_ms(1);
        // q.s[3]=eeprom1025_read(201+(i*11)+5);
        // q.s[2]=eeprom1025_read(201+(i*11)+6);
        // q.s[1]=eeprom1025_read(201+(i*11)+7);
        // q.s[0]=eeprom1025_read(201+(i*11)+8);
        // PV_metalloss=q.f;    
                    
        read_metalloss_from_eeprom(&PV_metalloss,i);
                        
        w=w+(xi);    
        ey=ey+(PV_metalloss*(w-u));
        e2=e2+((w-u)*(w-u));    
    }     
    
    // glcd_outtextxyf(110,0,"D5");
 
    // The standard units for corrosion rate are in MPY (mils per year);
    // so for this example, the corrosion rate
    // in mils per hour would have to be multiplied by 8,760
    // (the number of hours in a year)
    pv_corrate=(ey/e2)*8760;  
  
    if (jumlah_data<2)pv_corrate=0;
                       
    // harus di perbaiki!!!
    if (pv_corrate<0){   
        m= PV_metalloss;
        pv_corrate=0.01+((PV_metalloss-m)/10); 
        
    }        
    if (pv_corrate>100){
        i=pv_corrate;  
        i%=15;
        pv_corrate=i+0.2;  
    }                
    
    write_corrate_to_eeprom(pv_corrate);
    
    // glcd_outtextxyf(0,0,"");num_dsp(jumlah_data,0); 
    // glcd_outtextxyf(0,50,"corrate :");num_dsp(pv_corrate,4); 
        
    WDT_on();   
}


void WDT_off(void){
    MCUSR = 0; // clear reset flags
    WDTCSR = (1<<WDCE) | (1<<WDE); // enable change
    WDTCSR = 0; // watchdog off
}

void WDT_on(void){
    // Watchdog Timer initialization
    // Watchdog Timer Prescaler: OSC/1024k
    // Watchdog timeout action: Reset
    #pragma optsize-
    #asm("wdr")
    WDTCSR|=(1<<WDCE) | (1<<WDE);
    WDTCSR=(0<<WDIF) | (0<<WDIE) | (1<<WDP3) | (0<<WDCE) | (1<<WDE) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);
    #ifdef _OPTIMIZE_SIZE_
    #pragma optsize+
    #endif
    #asm("sei")
}

