
#include <glcd.h>
#include <arial28.h>
#include <arial8.h>

#define FW_VER "1.0.1"


void my_lcd_init(void);
void calculate_corrate(void);
void display_test_loop(void){
                    
                 
                    glcd_setfont(arial8);
                    glcd_clear();
                    glcd_outtextxyf(0,0,"LOOP TEST");
                    glcd_outtextxyf(0,10,"CURRENT :");num_dsp(PV_loop_current,2); 
}



void dsp_menu0(unsigned char type)
{
    my_lcd_init();
    glcd_clear();
    glcd_setfont(arial8);
                    
    EERead(27,&out_mode,1);

    if (out_mode>3)out_mode=0;  
    PV= PV_metalloss;
    
    if (out_mode>0)
    {
        output_loop_mode();  
        PV= pv_corrate;
    }
        
    // for transmitter?
    PV_loop_current=((16/(upper_range_value-lower_range_value))*PV)+4;
    if (PV_loop_current>20) PV_loop_current =20;
    if (PV_loop_current<4) PV_loop_current =4;
    
    if(type == 0)  
        glcd_outtextxyf(0,0,"METALLOSS :\n\r   ");    
    else
        glcd_outtextxyf(0,0,"CORR RATE :\n\r   ");
    
    glcd_setfont(arial28);
    glcd_printfxy(0,10,"%1.3f",PV_metalloss);

    //glcd_outtextf(" MILS\n\rCORRATE :\n\r   ");
    //num_dsp(pv_corrate,3);

    glcd_setfont(arial8);
    
    //num_dsp(PV_loop_current,2);glcd_outtextf(" mA");

    delay_ms(100);
    delay_ms(100);
}



void dsp_menu2(void)
{
my_lcd_init();
glcd_clear();
glcd_setfont(arial8);
glcd_outtextxyf(0,0,"SELECT MENU :");
glcd_outtextxyf(0,10,"  -TEST LOOP");
glcd_outtextxyf(0,20,"  -OUTPUT MODE");
//glcd_outtextxyf(0,30,"  -BACK");

glcd_outtextxyf(0,54,"ER-D");
glcd_outtextxyf(75,54,"v ");
glcd_outtextxyf(90,54,FW_VER);

delay_ms(200);

}

void dsp_menu3(void)
{
glcd_clear();
glcd_setfont(arial8);
glcd_outtextxyf(0,0,"LOOP TEST");
glcd_outtextxyf(0,10,"  -4 mA");
glcd_outtextxyf(0,20,"  -8 mA");
glcd_outtextxyf(0,30,"  -12 mA");
glcd_outtextxyf(0,40,"  -16 mA");
glcd_outtextxyf(0,50,"  -20 mA");
glcd_outtextxyf(62,10,"  -BACK");
delay_ms(200);


}

void dsp_menu4(void)
{
glcd_clear();
glcd_setfont(arial8);
glcd_outtextxyf(0,0,"OUTPUT MODE");
glcd_outtextxyf(0,10,"  -METALLOSS");
glcd_outtextxyf(0,20,"  -COR 10 MPY ");
glcd_outtextxyf(0,30,"  -COR 100 MPY ");
glcd_outtextxyf(0,40,"  -COR 1000 MPY ");
glcd_outtextxyf(0,50,"  -BACK ");

delay_ms(200);

}

void display_id(void){
uchar x;
glcd_setfont(arial8);
    glcd_clear();

 //   glcd_outtextf("\n\rRange : "); num_dsp(lower_range_value,0);
 //   glcd_outtextf("-");num_dsp(upper_range_value,1);glcd_outtextf(" mils\n\r");
    switch (probe_type)
    {
        case 10 :     //flush
            glcd_outtextf("FLUSH ");
        break;
        case 2 :      //tubular
            glcd_outtextf("TUBULAR ");
        break;
        case 3 :      //tubeloop
            glcd_outtextf("TUBELOOP ");
        break;
        case 4 :      //wireloop
            glcd_outtextf("WIRELOOP ");
        break;
    }
    num_dsp(upper_range_value,0);
}

void display_time(void)
{

num_dsp( tanggal,0); glcd_putchar('-');
num_dsp( bulan,0);   glcd_putchar('-');
num_dsp(tahun,0);    glcd_putchar(' ');
num_dsp( jam,0);     glcd_putchar(':');
num_dsp( menit,0) ;  
   
}

// buat text angka 
void num_dsp( float number,uchar koma){
unsigned long a;
unsigned long b;
unsigned long c;
unsigned char d;
unsigned char e;
float f;
unsigned char u;

    if (number<0) 
    {
        number=number*-1; 
        glcd_putchar('-');
    }  
A:
    a=number;
    b=1;
    d=0;
    u=0;
    do{
        b=b*10;
        c=a/b;
        d+=1; u++; if(u>10)c=0;
    }while(c>0);
    for(e=1;e<=d;e++){
        b=b/10;
        c=a/b;
        glcd_putchar(c+'0');
        a=a%b;
    }

    b=1;
    for(d=1;d<=koma;d++){
        b=b*10;
    }

    if(koma>0){
        glcd_putchar('.');  delay_ms(5);
        a=number;
        number=number-a;    
        
        for(d=1;d<=koma+1;d++){ 
                f=number*10;
                c=f;
                glcd_putchar(c+'0');   delay_ms(1);
                number=(number*10)-c;   
        }
    }


}

void my_lcd_init(void)
{

GLCDINIT_t glcd_init_data;

// Graphic Display Controller initialization
// The ST7565 connections are specified in the
// Project|Configure|C Compiler|Libraries|Graphic Display menu:
// DB0 - PORTC Bit 0
// DB1 - PORTC Bit 1
// DB2 - PORTC Bit 2
// DB3 - PORTC Bit 3
// DB4 - PORTC Bit 4
// DB5 - PORTC Bit 5
// DB6 - PORTC Bit 6
// DB7 - PORTC Bit 7
// A0 (RS, D /I) - PORTA Bit 2
// /RD - PORTA Bit 3
// /WR - PORTA Bit 4
// /CS - PORTA Bit 5
// /RES - PORTA Bit 6

// Specify the current font for displaying text
glcd_setfont(arial8);

// No function is used for reading
// image data from external memory
glcd_init_data.readxmem=NULL;

// No function is used for writing
// image data to external memory
glcd_init_data.writexmem=NULL;

// Set the LCD bias
glcd_init_data.lcd_bias=ST7565_DEFAULT_LCD_BIAS;

// Set horizontal display reverse state
glcd_init_data.reverse_x=ST7565_DEFAULT_REVX;

// Set to 1 for displays that use reversed RAM column address (reverse_x=1)
// driver and the pixel with x=0 is connected to column driver #132
glcd_init_data.rev132_x0=ST7565_DEFAULT_REV132_X0;

// Set vertical display reverse state
glcd_init_data.reverse_y=1;

// Set the V5 voltage regulator internal resistor ratio
glcd_init_data.volt_reg_v5=ST7565_DEFAULT_VOLT_REG_V5;

// Set the LCD driving mode
glcd_init_data.driving_mode=ST7565_DEFAULT_DRIVING_MODE;

// Set the LCD contrast
glcd_init_data.lcd_contrast=20;

glcd_init(&glcd_init_data);

glcd_setfont(arial8);

}



