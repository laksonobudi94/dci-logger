#ifndef _UKUR_ER_SEN_H
#define _UKUR_ER_SEN_H

#include <AD5274.h>
#include <AD7190.h>
//#include <general.h>
#include <stdio.h>
#include <math.h>

#define adc_cs PORTB.3
#define adc_sync PORTB.1
#define adc_drdy PINB.2
#define reg_on PORTB.4=1
#define reg_off PORTB.4=0

#define BC_pos ADC_WritePort(0x74); //delay_ms(200);
#define BC_neg ADC_WritePort(0x78); //delay_ms(200);
#define DC_pos ADC_WritePort(0x76); //delay_ms(200);
#define DC_neg ADC_WritePort(0x7A); //delay_ms(200);
#define EC_pos ADC_WritePort(0x75); //delay_ms(200);
#define EC_neg ADC_WritePort(0x79); //delay_ms(200);
#define ExC_off ADC_WritePort(0x00); //delay_ms(200);

void spi_init(void);
float rumus_metallos(unsigned char x,float thickness_val,float res_sens);

struct value_ukur
    {         
        float R_Sens;
        float R_Check;
        float PV_Metal_Loss;
        float ML_upper_limit;
        float ML_lower_limit;
    };
    

// hitung nilai current source
float data_to_current_value(unsigned char data);
// dengan mengecek linieritas voltage
int test_sensor_cable(int pos);
void spi_enable(void);

int ukur_er (struct value_ukur *hasil_ukur,
                unsigned char rumus,
                unsigned char thickness_val,
                unsigned char type_probe)
;
float rumus_metallos(unsigned char x,float thickness_val,float res_sens);

#endif