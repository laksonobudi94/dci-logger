/****************************************************************************
Font created by the LCD Vision V1.18 font & image editor/converter
(C) Copyright 2011-2015 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Font name: Lucida Console Bold
Fixed font width: 20 pixels
Font height: 31 pixels
First character: 0x20
Last character: 0x7F

Exported font data size:
8932 bytes for displays organized as horizontal rows of bytes
7684 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

#ifndef _LUCIDA24_INCLUDED_
#define _LUCIDA24_INCLUDED_

extern flash unsigned char lucida_console_bold_16[];

#endif

