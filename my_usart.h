#ifndef _MY_USART_H
#define _MY_USART_H

#include <mega644a.h>


#define RESPON_OK 1
#define VALID 1 
#define INVALID 0  

#define REPLY_DATA_CONFIG 101
#define REPLY_DOWNLOAD_HEADER 102
#define REPLY_DOWNLOAD_DATA 103

// USART0 Receiver interrupt service routine
interrupt [USART0_RXC] void usart0_rx_isr(void);
void send_command(unsigned char com,unsigned char byte_co,unsigned char probe_type);
void usart_init(void);

#endif
