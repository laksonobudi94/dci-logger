#include "LMP91000.h"   
#include <stdio.h>
            
uint8_t LMP91000_read(uint8_t reg){
      uint8_t chr = 0; 
      uint8_t no_response_from_ic;      
      
      //printf("i2c start\r\n");
      i2c_periph_start(LMP91000_I2C_ADDRESS);           // START+SLA+W
      //printf("i2c write\r\n");
      i2c_periph_write(reg);                                        // REG
      //printf("i2c stop\r\n");
      i2c_periph_stop();                            // REP START
      
      // Read         
      //printf("i2c read\r\n");
      i2c_periph_start(LMP91000_I2C_ADDRESS | 1);         // SLA+R
      //printf("i2c read nack\r\n");

      chr = i2c_read_nack(100,&no_response_from_ic); 
      if(no_response_from_ic)
        chr = 0xFF;
                                      // DATA
      //printf("dapet\r\n",chr);
      
      return chr;
}

uint8_t LMP91000_write(uint8_t reg, uint8_t data) {

	  i2c_periph_start(LMP91000_I2C_ADDRESS);               // START+SLA+W
      i2c_periph_write(reg);                                        // REG
      i2c_periph_write(data);                                       // DATA
      i2c_periph_stop(); // generate stop condition  // STOP

      // read back the value of the register
      return LMP91000_read(reg);
}



uint8_t LMP91000_status(void) {
      return LMP91000_read(LMP91000_STATUS_REG);
}      
      
uint8_t LMP91000_lock(){ // this is the default state
      return LMP91000_write(LMP91000_LOCK_REG, LMP91000_WRITE_LOCK);
}

uint8_t LMP91000_unlock(){ 
      return LMP91000_write(LMP91000_LOCK_REG, LMP91000_WRITE_UNLOCK);
}

uint8_t LMP91000_configure(uint8_t _tiacn, uint8_t _refcn, uint8_t _modecn){
      if(LMP91000_status() == LMP91000_READY){
            LMP91000_unlock();
            LMP91000_write(LMP91000_TIACN_REG, _tiacn);
            LMP91000_write(LMP91000_REFCN_REG, _refcn);
            LMP91000_write(LMP91000_MODECN_REG, _modecn);
            LMP91000_lock();
            return 1;
      }
      return 0;
}
