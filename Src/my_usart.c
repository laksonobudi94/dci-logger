#include <my_usart.h>
#include <delay.h>  
       
#include <stdio.h> 

extern unsigned char buffer_in[40];
extern unsigned char buffer_out[40];
        
unsigned char ptr;
unsigned char header;   

extern unsigned char byte_count;
extern unsigned char command_in; 
extern unsigned char respon;     
extern unsigned char read;
                
// USART0 Receiver interrupt service routine
interrupt [USART0_RXC] void usart0_rx_isr(void)
{

    unsigned char data;

    data=UDR0;    

    if (read==0)
    {
        buffer_in[ptr] = data;     
        
        if ( buffer_in[ptr-4]==255 && buffer_in[ptr-3]==255 && buffer_in[ptr-2]==255  && header==INVALID )
        {        
            command_in = buffer_in[ptr-1];
            byte_count = buffer_in[ptr]; 

            // format correct
            header = VALID;    
            ptr=0;
        }                                
        if (header==1)
        {          
            if (ptr==byte_count && respon==0)
            {
                respon = RESPON_OK;    
                header = INVALID; 
                ptr=0;
            }              
        }     
        ptr++;
    }                   
}


void send_command(unsigned char com,unsigned char byte_co,unsigned char type_probe)
{
    unsigned char x;

    header=0;
    respon=0;
    command_in=0;
    byte_count=0;
    ptr=0;
    delay_ms(50);

    // header
    putchar(255);  
    putchar(255);  
    putchar(255); 
             
    // commadn
    putchar(com);    
    putchar(byte_co);      
                 
    if (com == REPLY_DATA_CONFIG)
    {          
        if (type_probe<60)putchar(1);
        if (type_probe>=60)putchar(2);
    }         
    
    
    if (com == REPLY_DOWNLOAD_HEADER)
    {          
        for (x=0;x<byte_co;x++)
        {
            putchar(buffer_out[x]);                                        
        }
    }    

    if (com == REPLY_DOWNLOAD_DATA)
    {          
        for (x=0;x<byte_co;x++)
        {
            putchar(buffer_out[x]);                                        
        }
    }     
}



void usart_init(void)
{
// USART0 initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART0 Receiver: On
// USART0 Transmitter: On
// USART0 Mode: Asynchronous
// USART0 Baud Rate: 38400
UCSR0A=(0<<RXC0) | (0<<TXC0) | (0<<UDRE0) | (0<<FE0) | (0<<DOR0) | (0<<UPE0) | (0<<U2X0) | (0<<MPCM0);
UCSR0B=(1<<RXCIE0) | (0<<TXCIE0) | (0<<UDRIE0) | (1<<RXEN0) | (1<<TXEN0) | (0<<UCSZ02) | (0<<RXB80) | (0<<TXB80);
UCSR0C=(0<<UMSEL01) | (0<<UMSEL00) | (0<<UPM01) | (0<<UPM00) | (0<<USBS0) | (1<<UCSZ01) | (1<<UCSZ00) | (0<<UCPOL0);
UBRR0H=0x00;
UBRR0L=0x0C;
}
