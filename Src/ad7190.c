#include <ad7190.h>
#include <stdio.h>

#define DEBUG_ADC 0

/****************************************************************
 * Function Name : AD7190_reset
 * Description   : Resets the AD7190
 * Returns       : None
 * Params        : None
 ****************************************************************/
void ADC_Reset() 
{
    unsigned char x;
    adc_cs = 0;
    // adc_sync = 0;  
    
    for (x = 0; x < 10; x++) {
        spi(0xFF);
    }
    delay_ms(100);
    
    adc_cs = 1;
    // adc_sync = 1;     
}

/****************************************************************
 * Function Name : ADC_ReadData
 * Description   : Read Data ADC from AD7190
 * Returns       : None
 * Params        @timeout: timeout
 ****************************************************************/
int ADC_ReadData(float *volt_data)
{
    unsigned long reg_data; 
    unsigned long int hitung_drdy = 0;;
    float volt;
    bool sign_bit;

    // spi_init(0);

    adc_cs = 0;
    delay_ms(1);

    // adc_sync = 0;  
 
    spi(COM_WRITE_MODE_REG_CMD); // spi(0x08);
    
    // The mode register is a 24-bit register
    // Single conversion mode
    // Internal 4.92 MHz clock. Pin MCLK2 is tristated.
    spi(SET_MD0 | SET_CLK1); // spi(0x28);

    // Sinc3 filter select bit.
    // When this bit is set,
    // the sinc3 filter is used. The benefit of the sinc3 filter compared to the sinc4 filter is its lower settling time
    // when chop is disabled. For a given output data rate, fADC, the sinc3 filter has a settling time of 3/fADC
    // while the sinc4 filter has a settling time of 4/fADC.
    spi(SET_SINC3); // spi(0x80);
    
    // Filter output data rate select bits
    spi(SET_FS6 | SET_FS4); // spi(0x50);
     
    // delay_ms(200); 
    
    while (adc_drdy!=SUCCESS)
    {
        // notif for drdy failed
        if(hitung_drdy > TIMEOUT_DATA_RDY){
            #if (DEBUG_ADC)
                printf("-> ADC Read Failed : counter wait drd = %lu\n\r",hitung_drdy);
            #endif
            *volt_data = 0.0;
            return -1;   
        }
        else{ 
            hitung_drdy++;
        }
    }                  
    #if (DEBUG_ADC)
         printf("-> ADC Read OK : counter wait drd = %lu\n\r",hitung_drdy);
    #endif        
    
    // Code untuk memunculkan data ADC
    // setelah dapat signal DRDY
    spi(0x58);
    delay_ms(1);
 
    // 24 bit adc data value
    reg_data = spi(0xFF); // delay_ms(10);   
    reg_data<<=8;
    reg_data |= spi(0xFF); // delay_ms(10);     
    reg_data<<=8;
    reg_data |= spi(0xFF); // delay_ms(10); 
    reg_data<<=8;
    reg_data |= spi(0xFF); // delay_ms(10);    
    
    reg_data>>=8;   
    delay_ms(1);
    adc_cs = 1;
    delay_ms(1);
    
    // adc_sync = 1;  

    sign_bit=reg_data>>23;
    reg_data=reg_data&0x7FFFFF;  
    
    // voltage data
    volt=reg_data;
    volt=(volt/0x7fffff)*2.498;   
    if (!sign_bit)volt=2.498-volt;
        
    *volt_data = volt;      
    return 0;   
}

/****************************************************************
 * Function Name : ADC_ReadReg
 * Description   : Read Data from register AD7190
 * Returns       : None
 * Params        @reg: address register
 * Params        @reg_data: content data register
 ****************************************************************/
void ADC_ReadReg(unsigned char reg, unsigned char *reg_data)
{
    unsigned char length;

    /*
        Table 15. Register Selection
        RS2 RS1 RS0 Register Register Size
        0 0 0 Communications register during a write operation      8 bits
        0 0 0 Status register during a read operation               8 bits              v
        0 0 1 Mode register                                         24 bits
        0 1 0 Configuration register                                24 bits
        0 1 1 Data register/data register plus status information   24 bits/32 bits
        1 0 0 ID register                                           8 bits              v
        1 0 1 GPOCON register                                       8 bits              v
        1 1 0 Offset register                                       24 bits
        1 1 1 Full-scale register                                   24 bits 
    */
    length=3;
    if (reg==STATUS_REG ||
        reg==ID_REG ||
        reg==GPOCON_REG 
    )   length=1;

    // A 1 in this position
    // indicates that the next operation is a read from the designated register. 
    reg|=SET_CR6;
                  
    adc_cs = 0;
    
    //adc_sync = 0;
    // kirim address register yg mau di baca
    spi(reg); 

    // baca data register
    reg_data[0]= spi(0xFF);    
    
    if (length==3)
    {
        reg_data[1]= spi(0xFF);
        reg_data[2]= spi(0xFF);
    }
    
    adc_cs = 1;
    //adc_sync = 1;   
    printf("Reg Data = 0x%x\r\n",reg_data[0]);
}

/****************************************************************
 * Function Name : ADC_WriteReg
 * Description   : Write Data to Register AD7190
 * Returns       : None
 * Params        @reg: address register
 * Params        @reg_data: content data register 
 ****************************************************************/
void ADC_WriteReg(unsigned char reg, unsigned char *reg_data)
{
    unsigned char length;

     length=3;
     if (   reg==STATUS_REG ||
            reg==ID_REG ||
            reg==GPOCON_REG 
        )   length=1;
                  
     adc_cs = 0;

     // adc_sync = 0;
     spi(reg); 
     spi(reg_data[0]);    
     
     if (length==3)
        {
            spi(reg_data[1]);
            spi(reg_data[2]);
        }       
        
     delay_ms(100);
     adc_cs = 1;
     //adc_sync = 1;
}

/****************************************************************
 * Function Name : ADC_WritePort
 * Description   : kontroll GPIO from AD7190
 * Returns       : None
 * Params        @reg_data: content data register
 ****************************************************************/
void ADC_WritePort(unsigned char reg_data)
{
    adc_cs = 0;
    delay_ms(5);
    spi(GPOCON_REG); 
               
    // See Table 21. Register Bit Designations
    // GP7  GP6      GP5       GP4       GP3      GP2      GP1      GP0
    // 0(0) BPDSW(0) GP32EN(0) GP10EN(0) P3DAT(0) P2DAT(0) P1DAT(0) P0DAT(0)
                  
    // P1 & P0 for selector ADG1604 (Res Sensing)     
    // P3 & P2 for
    spi(reg_data);    
    delay_ms(5);
    adc_cs = 1;
    delay_ms(5);
    }

/****************************************************************
 * Function Name : ADC_InternalCal
 * Description   : Calibration internal ADC AD7190
 * Returns       : None
 * Params        : None
 ****************************************************************/
int ADC_InternalCal()
{
    unsigned long int hitung_drdy = 0;
    adc_cs = 0;
    delay_ms(10);
    // adc_sync = 0;  
 
    spi(COM_WRITE_MODE_REG_CMD); 
    
    // Internal zero-scale calibration.
    // Internal 4.92 MHz clock. Pin MCLK2 is tristated
    spi(SET_MD2 | SET_CLK1); //spi(0x88); 

    // Sinc3 filter select bit.
    // When this bit is set,
    // the sinc3 filter is used. The benefit of the sinc3 filter compared to the sinc4 filter is its lower settling time
    // when chop is disabled. For a given output data rate, fADC, the sinc3 filter has a settling time of 3/fADC
    // while the sinc4 filter has a settling time of 4/fADC.
    spi(SET_SINC3); //spi(0x80);

    // Filter output data rate select bits
    spi(SET_FS6 | SET_FS4); // spi(0x50);
     
    while (adc_drdy!=SUCCESS)
    {
        // notif for drdy failed
        if(hitung_drdy > TIMEOUT_DATA_RDY){
            #if (DEBUG_ADC)
                printf("-> ADC Calibrate Failed : counter wait drd = %lu\n\r",hitung_drdy);
            #endif
            return -1;   
        }
        else{ 
            hitung_drdy++;
        }
    }          
    #if (DEBUG_ADC)
        printf("-> ADC Calibrate OK : counter wait drd = %lu\n\r",hitung_drdy);
    #endif
    
    adc_cs = 1;
    delay_ms(10);
     
    adc_cs = 0;
    delay_ms(10);
    // adc_sync = 0;  
 
    spi(0x08); 

    // Internal 4.92 MHz clock. Pin MCLK2 is tristated   
    // Internal full-scale calibration
    spi(0xA8);

    // Sinc3 filter select bit. 
    spi(0x80);

    // Filter output data rate select bits
    spi(0x50);
              
    hitung_drdy = 0;      
    while (adc_drdy!=SUCCESS)
    {                          
        // notif for drdy failed
        if(hitung_drdy > TIMEOUT_DATA_RDY){
            #if (DEBUG_ADC)
                printf("-> ADC Calibrate Failed : counter wait drdy = %lu\n\r",hitung_drdy);
            #endif
            return -1;   
        }
        else{ 
            hitung_drdy++;
        }
    }
    #if (DEBUG_ADC)
        printf("-> ADC Calibrate OK : counter wait drd = %lu\n\r",hitung_drdy);
    #endif
    
    adc_cs = 1;
    delay_ms(10);     
                      
    return 0;
}
