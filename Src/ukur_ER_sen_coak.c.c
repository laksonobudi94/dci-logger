#include <ukur_ER_sen_coak.h>
#include "my_function.h"                   

bool kalibrasi=0;
float R_sen;
float ML_atas,ML_bawah;
 
extern unsigned int jumlah_data;      
extern unsigned char dsp_error_msg;


// dengan mengecek linieritas voltage
int test_sensor_cable(int pos){
    // 5 x test baca voltage dengan menaikan arus
    #define TEST_LINIER 3   
    //#define DEBUG_TEST_CABBLE

    unsigned char test_current;
    float volt_linier[TEST_LINIER];
    float test_read_adc;
    unsigned char higher;
    float BC,DC,EC;   
    float current_source;
    float resistance;  
    float cur_test;      
    unsigned char cur_int;
                                         
   // printf("MASUK TESTING KABEL \n\r");                              

    switch(pos){
        case 0:       
            BC_pos;
            break;
        case 1:       
            BC_neg;
            break;
        case 2:       
            DC_pos;
            break;
        case 3:       
            DC_neg;
            break;
        case 4:       
            EC_pos;
            break;
        case 5:       
            EC_neg;
            break;
        default:
            ExC_off;
            break;
    }    
             
    // Test Current                                             
    // 0.005mA 0.009mA 0.014mA       
    
//    cur_test = 0.005;
//    cur_int = current_to_data_value(&cur_test);
//    printf("cur_int %d | cur_test = %fmA\n\r",cur_int,cur_test);
//    cur_test = 0.009;
//    cur_int = current_to_data_value(&cur_test);
//    printf("cur_int %d | cur_test = %fmA\n\r",cur_int,cur_test);
//    cur_test = 0.014;
//    cur_int = current_to_data_value(&cur_test);
//    printf("cur_int %d | cur_test = %fmA\n\r",cur_int,cur_test);
    
//    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    cur_test = 0.003;
    for(test_current=1;test_current <= TEST_LINIER;test_current++){
        //current_source = data_to_current_value(test_current*20); 
                                              
        cur_int = current_to_data_value(&cur_test);
        cur_test=cur_test+0.003;
        //AD5274_wiper_write(cur_int); 
        AD527X_setSettingValue(cur_int);
        if(ADC_ReadData(&test_read_adc)!=SUCCESS){
            return -1;
        }    
        #ifdef DEBUG_TEST_CABBLE                                                            
            resistance=test_read_adc/current_source;
            if(pos==0)printf("BC_pos=%1.3f V %1.3f A %1.3f ohm[%d]\n\r ",test_read_adc,current_source,resistance,test_current-1);
            else if(pos==1)printf("BC_neg=%1.3f V %1.3f A %1.3f ohm[%d]\n\r",test_read_adc,current_source,resistance,test_current-1);
            else if(pos==2)printf("DC_pos=%1.3f V %1.3f A %1.3f ohm[%d]\n\r",test_read_adc,current_source,resistance,test_current-1);
            else if(pos==3)printf("DC_neg=%1.3f V %1.3f A %1.3f ohm[%d]\n\r",test_read_adc,current_source,resistance,test_current-1);
            else if(pos==4)printf("EC_pos=%1.3f V %1.3f A %1.3f ohm[%d]\n\r",test_read_adc,current_source,resistance,test_current-1);
            else if(pos==5)printf("EC_neg=%1.3f V %1.3f A %1.3f ohm[%d]\n\r",test_read_adc,current_source,resistance,test_current-1);
            else{}
        #endif
                                     
       volt_linier[test_current-1]=test_read_adc;
    }
//    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    
    ExC_off;
             
    higher = 0;
    for(test_current=1;test_current < TEST_LINIER;test_current++){
       // printf("%d %f %d %f\n\r",test_current-1,volt_linier[test_current-1],test_current,volt_linier[test_current]);

        if(volt_linier[test_current-1] < volt_linier[test_current]){
            higher++;                                             
           // printf("higher %d\n\r",higher);
        }
    }               
         
    //printf("higher %d\n\r",higher);
         
                
    if(higher >= TEST_LINIER-1)
        return 0;
    else
        return -1;

}

void spi_enable(void){
    SPCR=(0<<SPIE) | (1<<SPE) | (0<<DORD) | (1<<MSTR) | (1<<CPOL) | (1<<CPHA) | (0<<SPR1) | (1<<SPR0);
    SPSR=(0<<SPI2X);  
}      

    


/**
 *
 */
int8_t write_and_read_rdac (uint16_t data_16_to_write)
{               
    int16_t read_from_ad5274 = 0;
	int8_t status = 0;

	status += AD527X_command_write(RDAC_WRITE, data_16_to_write);
	read_from_ad5274 = AD527X_command_read(RDAC_READ, 0x00);
	printf("RDAC wrote 0x%x",data_16_to_write);
	printf(" read 0x%x\r\n",read_from_ad5274);

	return status;
}

int ukur_er (struct value_ukur *hasil_ukur,
                unsigned char rumus,
                unsigned char thickness_val,
                unsigned char type_probe)
{      
    unsigned char dat[4];
    float BC,DC,EC;
    float V_BC[3],V_DC[3],V_EC[3];
    float VA_BC,VB_BC,V_final_BC;
    float VA_DC,VB_DC,V_final_DC;
    float VA_EC,VB_EC,V_final_EC;
    
    float Metalloss_Value,ML_old;
       
    // float R_cek;
    float m;
    unsigned char wiper;
    int16_t cek_wiper,cek_reg;
    unsigned char x;  unsigned char y;unsigned char c;
    unsigned char data;      
    unsigned char Controll_Reg_AD5274; 
    unsigned char id_register; 
    unsigned char cek_kali_baca = 0;     
    

    float read_adc;
              
  
    spi_enable();                  
   
    // glcd_clear();

    R_sen=1.005;      
    //metal_loss_value=thickness_val*(1-(1/res_sens));
    //10*(1-(1/1.005)
    //10*(1-(0.995)
    //10*0.0049
    //0.049      
    Metalloss_Value = rumus_metallos(rumus,thickness_val,R_sen);    
    ML_old = ML_atas;
                           
    //printf("MLVAL ===> %f\r\n",Metalloss_Value);
    ML_bawah = Metalloss_Value;
    ML_atas = ML_atas + ML_bawah;
    ML_bawah = ML_atas - (ML_bawah + ML_bawah);


    for (y=0;y<10;y++)
    {        
 
    if (kalibrasi==0)
        {    
            kalibrasi=1; 
            reg_on; 
            delay_ms(200);
            ADC_Reset(); 
            dat[0]=0x80;
            dat[1]=0x10;
            dat[2]=0x00; 
            ADC_WriteReg(CONFIG_REG, dat);
                          
            printf("ADC_InternalCal... ");
            if(ADC_InternalCal()!=SUCCESS)  
            {              
                kalibrasi=0;
                printf("[FAILED]\r\n");
                return DRDY_ADC_FAIL;
            }                                      
            printf("[OK]\r\n");
                   
            // 0 kan dulu ntar bandingin sama ga
            dat[0]=0;
            dat[1]=0;
            dat[2]=0;           
            ADC_ReadReg(CONFIG_REG,dat); 
            if(dat[0]==0x80 && dat[1]==0x10&&dat[2]==0x00){}
            else {
                kalibrasi=0;
                printf("dat [0] 0x%x [1] 0x%x [2] 0x%x\r\n",dat[0],dat[1],dat[2]);
                return CONF_REG_ADC_FAIL;                                      
            }
            
            SET_ULANG:
            //AD5274_on();
            AD527X_Init(8,20,AD5274_addr);
            AD527X_begin();     
            if(AD527X_control_write_verified(0)!=0)
            {            
                kalibrasi=0;
                return WIPER_CONF_FAIL;            
            }  

            if(AD527X_control_write_verified(RDAC_WIPER_WRITE_ENABLE)!=0)
            {            
                kalibrasi=0;
                return WIPER_CONF_FAIL;            
            }  
            
            if(AD527X_command_write(CONTROL_WRITE,RDAC_WIPER_WRITE_ENABLE) == 0){
                kalibrasi=0;
                return WIPER_CONF_FAIL;            
            }       
            
            cek_reg = AD527X_command_read(CONTROL_READ,0);
            if(cek_reg & RDAC_WIPER_WRITE_ENABLE)
                printf("Unlock Successfull\r\n");
            else                
                printf("Unlock Failed\r\n");

            cek_reg = AD527X_command_read(RDAC_READ,0);
            printf("RDAC read 0x%x\r\n");
            
                           
//            if(read_config_reg() !=SUCCESS){
//                printf("AD5274 fail\n");
//                goto SET_ULANG;                 
//            }
        
            // LAKSONO       
            // test cabble    
            if(test_sensor_cable(0)!=SUCCESS){kalibrasi=0;return BC_READ_FAIL;}
            if(test_sensor_cable(2)!=SUCCESS){kalibrasi=0;return DC_READ_FAIL;}
            if(test_sensor_cable(4)!=SUCCESS){kalibrasi=0;return EC_READ_FAIL;}
            
        }
          
    // RWA = 3906.25 ohm    
    // I_out = 7.82 mA
    //AD5274_wiper_write(50); 
    
    write_and_read_rdac(50);
    //AD527X_setSettingValue(50);     
    //cek_wiper = AD527X_getSetting();    
    //printf("cek_wiper %d\r\n",cek_wiper);
                                
//    if(cek_wiper < 0) 
//    {
//        kalibrasi=0;
//        return WIPER_CONF_FAIL;            
//    }  
    
    BC_neg          

    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }

    BC=read_adc;
                   
    // in probe dummy --> BC = 0.868
    printf("BC=%1.3f ",BC);  
    BC_pos
    // in probe dummy read_adc(BC) = 0.97
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    
    BC+=read_adc;
                                       
    // in probe dummy --> BC=1.838
    printf("BC=%1.3f \n\r",BC);
    
    // in probe dummy --> BC/2 = 0.919
    BC/=2;  // rata-rata nilai teg BC

    //?
    m=(0.9/BC)*50;        

    // limit RWA(D) register in 8 bit
    if (m>255)m=255;

            
    // hilangin coma
    // 48.96
    // change to 48
    wiper=m;            
    
    // Rset = 3.75 Kohm	Iout = 7.51 mA
    //AD5274_wiper_write(wiper);        
    //AD527X_enableWiperPosition(); 
    //AD527X_setSettingValue(wiper);
    //cek_wiper = AD527X_getSetting();
    //data = AD5274_wiper_read();     
    write_and_read_rdac(wiper);
    //printf("data = %d  ",cek_wiper); 
    
    // value incorrect readback                            
    // skip dulu!
    //if(wiper != data)return -2;
    
    BC=0;
    DC=0;
    EC=0;
            
    // Current-Reversal Method
        BC_neg           
        if(ADC_ReadData(&read_adc)!=SUCCESS){
            kalibrasi=0;
            return DRDY_ADC_FAIL;
        }

        m=read_adc;
        printf("%1.3f ",m);     
        
    BC+=m;
        BC_pos  
        if(ADC_ReadData(&read_adc)!=SUCCESS){
            kalibrasi=0;
            return DRDY_ADC_FAIL;
        }

        m=read_adc;
        printf("%1.3f ",m);         
    BC+=m;
        DC_pos  
        if(ADC_ReadData(&read_adc)!=SUCCESS){
            kalibrasi=0;
            return DRDY_ADC_FAIL;
        }

        m=read_adc;
        printf("%1.3f ",m);      
    DC+=m;
        DC_neg  
        if(ADC_ReadData(&read_adc)!=SUCCESS){
            kalibrasi=0;
            return DRDY_ADC_FAIL;
        }

        m=read_adc;
        printf("%1.3f ",m);     
    DC+=m;
        EC_pos  
        if(ADC_ReadData(&read_adc)!=SUCCESS){
            kalibrasi=0;
            return DRDY_ADC_FAIL;
        }

        m=read_adc;
        printf("%1.3f ",m);   
    EC+=m;         
        EC_neg     
        if(ADC_ReadData(&read_adc)!=SUCCESS){
            kalibrasi=0;
            return DRDY_ADC_FAIL;
        }

        m=read_adc;         
        printf("%1.3f ",m);   
                            
    EC+=m;   
          
    #if 0
        
    /*****************************************************************
     *  Delta Method  
     *  ambil 3x data pengukuran tegangan tiap titik
     *  Voltage BC 3x,DC 3x, Ec 3x    
     *   
     *  First, take one-half 
     *  the difference of the first two voltage measurements
     *  and call this term VA  
     *                 
     *  VA_BC = (+BC_0 - BC_1)/2        
     *   
     *  Then,  take  one-half  the  difference  of  the  second  (VM2)
     *  and  third  (VM3) voltage measurements and call this term VB
     *   
     *  VB_BC = (+BC_2 - BC_1)/2    
     *  
     *  The final voltage reading is the average of VA and VB
     *  and is calculated as                        
     *       
     *  V_final_BC = (VA_BC - VB_BC)/2
     *****************************************************************/      
               
   //  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");  
    /*************************
     * Voltage BC Measurement
     *************************/
                       
    // titik pengukuran pertama teg BC + 
    BC_pos  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_BC[0]=read_adc;
  
    // titik pengukuran kedua teg BC - 
    BC_neg  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_BC[1]=read_adc;
  
    // titik pengukuran ketiga teg BC + 
    BC_pos  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_BC[2]=read_adc;
                              
    VA_BC = (V_BC[0]+V_BC[1])/2.0;
    VB_BC = (V_BC[2]+V_BC[1])/2.0;
    V_final_BC = (VA_BC+VB_BC)/2.0;   
                
    /*************************
     * Voltage DC Measurement
     *************************/
    
    // titik pengukuran pertama teg DC + 
    DC_pos  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_DC[0]=read_adc;
  
    // titik pengukuran kedua teg DC - 
    DC_neg  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_DC[1]=read_adc;
  
    // titik pengukuran ketiga teg DC + 
    DC_pos  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_DC[2]=read_adc;
                              
    VA_DC = (V_DC[0]+V_DC[1])/2.0;
    VB_DC = (V_DC[2]+V_DC[1])/2.0;
    V_final_DC = (VA_DC+VB_DC)/2.0;   
                                          
    /*************************
     * Voltage DC Measurement
     *************************/   
    
    // titik pengukuran pertama teg EC + 
    EC_pos  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_EC[0]=read_adc;
  
    // titik pengukuran kedua teg EC - 
    EC_neg  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_EC[1]=read_adc;
  
    // titik pengukuran ketiga teg EC + 
    EC_pos  
    if(ADC_ReadData(&read_adc)!=SUCCESS){
        kalibrasi=0;
        return DRDY_ADC_FAIL;
    }
    V_EC[2]=read_adc;
                              
    VA_EC = (V_EC[0]+V_EC[1])/2.0;
    VB_EC = (V_EC[2]+V_EC[1])/2.0;
    V_final_EC = (VA_EC+VB_EC)/2.0; 

//    printf("V_BC[0] = %f\r\n",V_BC[0]);  
//    printf("V_BC[1] = %f\r\n",V_BC[1]);  
//    printf("V_BC[2] = %f\r\n",V_BC[2]);  
//    printf("VA_BC = %f\r\n",VA_BC);  
//    printf("VB_BC = %f\r\n",VB_BC);  
//    printf("V_final_BC = %f\r\n",V_final_BC);  
//                                         
//    printf("V_DC[0] = %f\r\n",V_DC[0]);  
//    printf("V_DC[1] = %f\r\n",V_DC[1]);  
//    printf("V_DC[2] = %f\r\n",V_DC[2]);  
//    printf("VA_DC = %f\r\n",VA_DC);  
//    printf("VB_DC = %f\r\n",VB_DC);  
//    printf("V_final_BC = %f\r\n",V_final_DC);  
//
//    printf("V_EC[0] = %f\r\n",V_EC[0]);  
//    printf("V_EC[1] = %f\r\n",V_EC[1]);  
//    printf("V_EC[2] = %f\r\n",V_EC[2]);  
//    printf("VA_EC = %f\r\n",VA_EC);  
//    printf("VB_EC = %f\r\n",VB_EC);  
//    printf("V_final_EC = %f\r\n",V_final_EC);  
//    
    BC = V_final_BC;
    DC = V_final_DC;
    EC = V_final_EC;   
    
// printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");  
    #endif

    
    ExC_off           
    
    R_sen=BC/DC;
    hasil_ukur->R_Check=(EC-DC)/DC;     
    
    // glcd_printfxy(0,20,"%1.3f %1.3f %1.3f ",BC,DC,EC);
    // glcd_printfxy(0,30,"%1.3f %1.3f ",R_sen,R_cek);
    // delay_ms(3000);

    kalibrasi=0;    
                               
    x=type_probe/10;
    if (x==5)x=1;   
    // glcd_printfxy(0,40,"probe_type = % 3d",probe_type); 

    Metalloss_Value = rumus_metallos(x,thickness_val,R_sen);           

    if (hasil_ukur->R_Check>0.290 && hasil_ukur->R_Check< 0.300  &&   Metalloss_Value>ML_bawah  &&  Metalloss_Value<ML_atas)
    //if (hasil_ukur->R_Check>0.285 && hasil_ukur->R_Check< 0.302  &&   Metalloss_Value>ML_bawah  &&  Metalloss_Value<ML_atas)
       {     
            c=y;
            break;
       }  
       else cek_kali_baca++;

    }    
    //delay_ms(3000) ;


    printf("\n\r%1.3f %1.3f %1.3f %d\n\r",R_sen, hasil_ukur->R_Check, Metalloss_Value,y);  
    printf("%1.3f %1.3f %1.3f\n\r",BC,DC,EC);  
    printf("kali baca %d ML_UP %f ML_DW %f\n\r",cek_kali_baca,ML_bawah,ML_atas);  
                               
    hasil_ukur->R_Sens = R_sen;
    hasil_ukur->PV_Metal_Loss = Metalloss_Value; 
    hasil_ukur->ML_upper_limit = ML_atas;
    hasil_ukur->ML_lower_limit = ML_bawah;  
      
    // di pembacaan pertama pasti 10x
    // dan akan selalu   
    // some time ini akan terjadi & jika     
    if(jumlah_data != 0 && cek_kali_baca >= 10){
        return TEN_TIME_FAIL;             
    }    
         
    return 0;
}

float rumus_metallos(unsigned char x,float thickness_val,float res_sens)
{
    float metal_loss_value;
    switch (x)
    {
        case 1 :     //flush
            metal_loss_value=thickness_val*(1-(1/res_sens));
        break;
        case 2 :      //tubular
            metal_loss_value=(160.5)-sqrt(((160.5-thickness_val)*(160.5-thickness_val))+(((321*thickness_val)-(thickness_val*thickness_val))/res_sens));
        break;
        case 3 :      //tubeloop
            metal_loss_value=(20)-sqrt(((20-thickness_val)*(20-thickness_val))+(((40*thickness_val)-(thickness_val*thickness_val))/res_sens))    ;
        break;
        case 4 :      //wireloop
            metal_loss_value=(thickness_val/2)*(1-sqrt(2/res_sens));
    }     
    
    return metal_loss_value;    
}
