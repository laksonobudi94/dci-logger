/*
 * _24lc1024.c
 *
 * Created: 4/14/2020 11:22:09 AM
 *  Author: Rahadi
 */ 
//#include "i2c.h"
#include "24lc1025_twi.h"
#include "twi_periph.h"     

#include <delay.h>

int init_eeprom(void){
	
	//i2c_periph_init();
	
	return 0;
}

uint8_t EEWrite(uint16_t u16addr, uint8_t *u8data, int len)
{
	
	
	if(i2c_writeReg16(EEDEVADR,u16addr,u8data,len)==1){
		delay_ms(10);
		return ERROR;
	}else{
		delay_ms(10);
		return SUCCESS;
	}
}

uint8_t EERead(uint16_t u16addr, uint8_t *u8data,int len)
{
	if(i2c_readReg16(EEDEVADR,u16addr,u8data,len)==1){
		delay_ms(10);
		return ERROR;
	}	
	else{
		delay_ms(10);
		return SUCCESS;
	}
	
	
	
}