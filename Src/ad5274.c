#include <ad5274.h>
#include <stdio.h>
#include <inttypes.h>

uint8_t BaseAddr;
uint32_t _MaxResistance;
uint8_t _base;         
uint8_t _bytes;
uint8_t error_count = 0;
uint16_t _maxValue;
uint16_t _value;
uint16_t _minValue = 0;
uint32_t _resistance;

/**
 * Constructor 
 * @param bits of the IC | 8 or 12 |
 * @parma Resistance value | 20, 50, 100 |
 * @param I2C base address 
 * 
 */            
void AD527X_Init(uint8_t bytes, uint8_t resistance, uint8_t base)
{
    _bytes = bytes;
    _base = base;
	_MaxResistance = resistance;
    BaseAddr = base;
}


/*
    Write commands to the AD527X
    COMMANDS: 
    RDAC_WRITE write lsb 10- or 8- bit data to RDAC (Must unlock first!)
    TP_WRITE store RDAC value to next 50-TP location (it must be unlocked to allow this)
    RDAC_REFRESH (software rest) 
    CONTROL_WRITE write data bits 2:0 to the control register
    SHUTDOWN set data bit 0 to shutdown, clear it for normal operation

    @param command - the type of write command 

    @param write_datum16 - the data we will use if the command supports. 10 lsb are used, we clip off any data in bits 15..10

    @return boolean if command and data were written
 */
uint8_t AD527X_command_write(uint8_t command, uint16_t write_datum16){
    uint8_t error_count = 0;
    uint8_t data = 0;
    uint8_t multiplier = 0; 
    uint16_t step = 0;
    int8_t return_val = 0;
    int8_t count = 0;
    uint16_t data_to_write = 0;
    uint8_t flag = 1;

    if (write_datum16 > 0x3FF)
    {
        // data in bits 13:10 will clobber the command when we OR in write_datum16
        write_datum16 &= 0x3FF;    // clip off any bad high bits even though we are going to error out and not use them
        // this is an error in the code using this function, we should not proceed with the write
        error_count |= 0x10;
    }

    if ( (RDAC_WRITE == command) || (CONTROL_WRITE == command) || (SHUTDOWN == command) )
    {
        // these commands need to use data we send over
        // shift the command over into bits 13:10
        data_to_write = command<<10;

        // also need to send 10-bit or 8-bit wiper value, or 3 control bits, or shutdown bit
        data_to_write |= write_datum16;
    }
    else if ( (TP_WRITE == command) || (RDAC_REFRESH == command) )
    {
        // shift the command over into bits 13:10
        data_to_write = command<<10;
        // no data needed
    }
    else
    {
        // It's either a bad command (out of range, > SHUTDOWN), or its not a writeable command
        // Bad command, we can't reasonably proceed
        error_count |= 0x20;
    }

    // if no errors so far, command is valid and datum is too
    if (0 == error_count)
    {   
        i2c_periph_start(_base);
        data = (data_to_write >> 8);        // ms byte to write
        if(i2c_periph_write(data)==0)count=1;      
		data = data_to_write & 0x0FF;		// ls byte to write
		
        if(i2c_periph_write(data)==0)count++;      
		if (count!=2) error_count += (2-count);	// add number of bad writes
                
        i2c_periph_stop();
        error_count+=0;
	}

	if(RDAC_WRITE == command){
		//_value = readRDAC();
	}

	// if errors, return the count, make it negative first
	if (error_count > 0)
		{
		flag = 0;
		}

	return flag;
}

/*
	Helper function to set limit of setting
 */
uint16_t AD527X_limit(uint16_t value){
	if(value <= _minValue) {
		return _minValue;
	}else if(value >= _maxValue){
		return _maxValue;
	}else 
		return value;
}

/**
 * Write to the control register, then read it back and verify
 * the read data matches the write data.
 *
 * @param control only 3 lsb are used, others ignored but if nonzero increments error count.
 * This can be a value or a combination of manifest constants. The default, POR value for
 * these bits is 000
 *
 * @see TP_WRITE_ENABLE
 * @see RDAC_WIPER_WRITE_ENABLE
 * @see RDAC_CALIB_DISABLE
 *
 * @return 0 if successful, nonzero for total number of errors which occurred
 *
 * @TODO this seems redundant. Instead reuse the more generic command_write function.
 */
int8_t AD527X_control_write_verified(uint8_t control)
{
	uint8_t error_count = 0;
	uint8_t data = 0;

	uint8_t recvd = 0xFF;	// init with impossible value
	int8_t count = 0;
	uint16_t data_to_write = 0;   
    uint8_t no_response_from_ic;

	if (control > 7)	// bad value, what is caller's intent?
	{
		// AD5274 will ignore all bits except 2:0 so we can proceed
		// but this is still not good, so bump the error counter
		error_count++;
	}
	data_to_write = (CONTROL_WRITE<<10) | control;
	i2c_periph_start(_base);
	data = (data_to_write >> 8);		// ms byte to write
	if(i2c_periph_write(data)==0)count=1;
	data = data_to_write & 0x0FF;		// ls byte to write
	if(i2c_periph_write(data)==0)count++;
	if (count!=2) error_count += (2-count);	// add number of bad writes
                             
    i2c_periph_stop(); 
    error_count+=0;

    
	// now see if control reg was correctly written by reading it

	// write the control read command, data except the command value are don't cares
	data_to_write = CONTROL_READ<<10;
	i2c_periph_start(_base);
	data = (data_to_write >> 8);		// ms byte to write
	if(i2c_periph_write(data)==0)count=1;
	data = data_to_write & 0x0FF;		// ls byte to write
	if(i2c_periph_write(data)==0)count++;
	if (count!=2) error_count += (2-count);	// add number of bad writes
                 
    i2c_periph_stop(); 
    error_count+=0;


	// now request the read data from the control register
    if(i2c_periph_start(_base | READ_REG)==0)count=2;
	
	if (2 != count)
	{
		printf(" Error: I2C control read didn't return 2 but %d\r\n",count);
		error_count += (2-count);	// add number of bad reads
	}
	recvd = i2c_read_ack(100,&no_response_from_ic);	// ms byte is don't care, discard this
    if(no_response_from_ic)
        return -1;
        
	recvd = i2c_read_nack(100,&no_response_from_ic);	// ls byte bits 2:0 holds the control reg value
    if(no_response_from_ic)
        return -1;

	if (recvd != control)
		{
			printf(" Error: Control reg write and read don't match: 0x%x %\r\n",control);
			printf("0x%x",recvd);
		}

	//printf("Control register value now 0x%x",recvd);

	return error_count;
}


/*
	Command number 7: This command unlocks the RDAC register to control potentiometer.
	Only call this function after initialisation phase. Enables wiper position change bit.
 */
void AD527X_enableWiperPosition(){
	AD527X_control_write_verified(RDAC_WIPER_WRITE_ENABLE);
}

/**
 * Send a command read and request data from the AD5274
 * To read, we send a  read command and (possibly) needed location data
 *
 * @param command valid values are
 * RDAC_READ - reads current wiper value
 * TP_WIPER_READ also needs six bits of location data (which of 50, 0x01 to 0x32)
 * TP_LAST_READ - read the most recently programmed 50-TP location
 * CONTROL_READ - read the value of the control register, in 4 lsbs
 *
 * @param write_datum is only used with read of 50-TP wiper memory, and only six lsb are used
 *
 * @return if positive it is read data; at most the 10 lsb are meaningful
 * if negative, it's the count of errors where more negative is more errors
 * if -100 the only error is "bad command", and no read was attempted
 * if a smallish but negative number there were I2C errors on attempted read, and the absolute value is the error count
 *
 *
 */
int16_t AD527X_command_read(uint8_t command, uint8_t write_datum)
{
	uint8_t error_count = 0;
	uint8_t data = 0;
	uint16_t data_to_write = 0;
	int16_t read_datum = 0;

	// this will hold two data bytes as they are read
	uint8_t recvd = 0xFF;	// init with impossible value
	int8_t count = 0;

	if (TP_WIPER_READ == command)
	{
		// shift the command over into bits 13:10
		data_to_write = command<<10;
		// also need to send 6-bit 50-TP location
		data_to_write |= write_datum;
	}
	else if ( (RDAC_READ == command) || (TP_LAST_USED == command) || (CONTROL_READ == command) )
	{
		// command is in range and something possible to read
		// shift the command over into bits 13:10
		data_to_write = command<<10;
	}
	else
	{
		// It's either a bad command (out of range, > SHUTDOWN), or its not a readable command
		// Bad command, we can't reasonably proceed
		error_count = 100;
		read_datum = error_count * -1;
	}

	/**
	 * At this point, if error_count == 0 we have a valid read command
	 */
	if (0 == error_count)
	{
//		Serial.println(" No errors so far");
		i2c_periph_start(_base);		// I2C slave address
		data = (data_to_write >> 8);		// ms byte to write
		if(i2c_periph_write(data)==0)count=1;
		data = data_to_write & 0x0FF;		// ls byte to write
		if(i2c_periph_write(data)==0)count++;
		if (count!=2) error_count += (2-count);	// add number of bad writes
        i2c_periph_stop();   
        error_count+=0;

       
        
//		Serial.print(" Error count after command write=");
//		Serial.println(error_count);

		// now request the read data from the control register
		if(i2c_periph_start(_base | READ_REG)==0)count=2;
                        
        if (2 != count)
		{
			printf(" Error: I2C command %d",command);
			printf(" read didn't return 2 but %d",count);                     
            
            return -100;
            
			if (count!=2) error_count += (2-count);	// add number of bad reads
		}            
        
		recvd = i2c_read_ack(100,NULL);	// ms byte
		read_datum = recvd << 8;

		recvd = i2c_read_nack(100,NULL);	// ls byte
		read_datum |= recvd;

		// if errors, return the count, make it negative first
		if (error_count > 0)
			{
			//Serial.print(" Error count after command_read =");
			//Serial.println(error_count);
			read_datum = -1 * error_count;
			}
	}

	return read_datum;
}

/*
	Command 1 from Datasheet
	Reads the current setting of the RDAC register.
	@return if positive it is read data; at most the 10 lsb are meaningful
   if negative, it's the count of errors where more negative is more errors
   if -100 the only error is "bad command", and no read was attempted
   if a smallish but negative number there were I2C errors on attempted read, and the absolute value is the error count
 */
int AD527X_getSetting(){
	return AD527X_command_read(RDAC_READ, 0);
}

void AD527X_shutdown(){
	AD527X_command_write( SHUTDOWN,  0);
}

/*
	Sets new value into RDAC register, and changes wiepr position.
	This command is supported only after enabling the wiper position change.
	@param inputValue 8 bit value to set wiper position
 */
void AD527X_setSettingValue(uint16_t inputValue){ // done
	_value = AD527X_limit(inputValue);
	AD527X_command_write( RDAC_WRITE,  _value); 
}

/*
@return 0 if no I2c Errors, else the error count, also opens RDAC wiper
@param resistance: 20, 50, 100
 */
uint8_t AD527X_begin(void){ 
    // printf("_bytes %d _MaxResistance %d\r\n",_bytes,_MaxResistance);
    if(_bytes == 8){ // sets byte size 
        _maxValue = 255;
        _value = 255;
//	}else if(_bytes == 12){ // kok 12?
	}else if(_bytes == 10){
		_maxValue = 1023;
		_value = 1023;
	}else{
		error_count++;
	}

	_MaxResistance = _MaxResistance * 1000;

    // disini dong harusnya    
    if(_MaxResistance != 20000 || _MaxResistance != 50000 || _MaxResistance != 100000) // if resistance value not valid, increase error
    error_count++;

	//AD527X_command_write(CONTROL_WRITE, RDAC_WIPER_WRITE_ENABLE);
    
	//AD527X_setSettingValue(_value);
	return error_count;
}

/*
	Increment wiper setting by 1, if at max or min, stays at those positions
 */
void AD527X_increment(){ // increment by 1
	_value = AD527X_limit((_value+1));
	AD527X_setSettingValue(_value);
}

/*
	Decrement wiper setting by 1, if at max or min, stays at those positions
 */
void AD527X_decrement() { // decrement by 1
	_value = AD527X_limit((_value-1));
	AD527X_setSettingValue(_value);
}            

/*
	Gets max setting
 */
uint16_t AD527X_getMaximumSetting(){ 
	// TODO::go through the IC and try to increment until we get to max??
	return _maxValue;
}

// sets rheostat max resistance 
/* 
void AD527X::setMaxResistance(uint32_t setMaxResistance){
	_MaxResistance = setMaxResistance;
}
*/

// _value = (R/Rmax)*1024
/*
	Set resistance of the IC
	setting = (Resistance/Rmax)*1023
 */
void AD527X_setResistance(uint32_t targetResistance){
	_value = AD527X_limit(((float)targetResistance)/((float)_MaxResistance)*1024);
	//Serial.println(_value);
	_resistance = (((float)_value)/((float)1023))*_MaxResistance;
	AD527X_setSettingValue(_value);
}	

// return rounded targetResistance
uint32_t AD527X_getResistance(){
	_resistance = ((float)_value/(float)1023)*_MaxResistance;
	return _resistance;
}



#if 1


/****************************************************************
 * Function Name : AD5274_wiper_read
 * Description   : 
 * Returns       : None
 * Params        : None
 ****************************************************************/
unsigned char AD5274_wiper_read(void) {
    unsigned char addr;
    unsigned char data;
    unsigned char data1;   
    
    i2c_periph_start(AD5274_addr);

    // Please read :
    // Table 12. Command Operation Truth Table
    // Read contents of RDAC wiper register
    i2c_periph_write(RDAC_wiper_register);    //  0 0 C3 C2 C1 C0 D9 D8
    i2c_periph_write(0x00);                   //  D7 D6 D5 D4 D3 D2 D1 D0
    i2c_periph_stop();

    // read register content
    i2c_periph_start(AD5274_addr | READ_REG);
    
    // The RDAC register
    // contents determine the resistor wiper position
    data=i2c_read_ack(100,NULL);
    data1=i2c_read_nack(100,NULL);
    i2c_periph_stop();
    
    data=data<<6;
    data1=data1>>2;
    data=data|data1;
    return data;
}

/****************************************************************
 * Function Name : AD5274_wiper_write
 * Description   : 
 * Returns       : None
 * Params        : None
 ****************************************************************/
void AD5274_wiper_write(unsigned char data) {

    // The nominal resistance between Terminal W and Terminal A, RWA,
    // is available in 20 kohm, 50 kohm, and 100 kohm, and 1024-/256-tap points
    // accessed by the wiper terminal

    // For the AD5274
    // Rwa(D) = (D/256)*Rwa    
    // AD5274BRMZ-20-RL7 --> Rwa = 20 Kohm
    
    unsigned char addr;
    unsigned char x;
                      
    // I_out    =   V_set/R_out
    //          =   (10uA * R_set)/R_out
    // Rout = 4.99 ohm
    // Rset = R_wa (from AD5274)

                                  
    // limit RWA = 14843.75 ohm?
    // limit Iout = 29.7 mA
    if(data>190)data=190; //?
    // RWA = 19921.875  ohm
    // limit Iout 39.92 mA
    // if(data>255)data=255;  

    addr=AD5274_addr;

    // Send D9 D8
    x=data>>6;
    
    // Table 12. Command Operation Truth Table
    // Write contents of serial register
    // data to RDAC. 
    x=x|0x04;          
     
    i2c_periph_start(AD5274_addr | WRITE_REG);
    // Send D9 D8
    i2c_periph_write(x);
    // Send D7 - D0
    i2c_periph_write(data<<2);
    i2c_periph_stop();
    delay_ms(10);
}

/****************************************************************
 * Function Name : AD5274_on
 * Description   : turn on AD5274
 * Returns       : None
 * Params        : None
 ****************************************************************/
void AD5274_on(void) {

    i2c_periph_start(AD5274_addr | WRITE_REG);

    // Please read :
    // Table 12. Command Operation Truth Table
    // Software shutdown.
    // normal mode
    i2c_periph_write(Software_shutdown);       //  0 0 C3 C2 C1 C0 D9 D8
    i2c_periph_write(Normal_Mode);             //  D7 D6 D5 D4 D3 D2 D1 D0
    i2c_periph_stop();
    delay_ms(10);
                  
    i2c_periph_start(AD5274_addr | WRITE_REG);
    // Please read :
    // Table 12. Command Operation Truth Table
    // Write contents of the serial
    // register data to the control
    // register.          
                  
    i2c_periph_write(SET_C2 | SET_C1 | SET_C0);    //  0 0 C3 C2 C1 C0 D9 D8
    // enable device for 50-TP program
    // allow update of wiper position through a digital interface
    // RDAC resistor tolerance calibration disabled 

    // 3rd programmed wiper location (0x03)
    i2c_periph_write(SET_D1 | SET_D0);    //  D7 D6 D5 D4 D3 D2 D1 D0

    i2c_periph_stop();
    delay_ms(10);  

}

/* This function for test Wiper*/
int read_config_reg(){

    unsigned char status[2];
    
    i2c_periph_start(AD5274_addr | WRITE_REG);

    // Please read :
    // Table 12. Command Operation Truth Table
    // Read contents of RDAC wiper register
//    i2c_periph_write(CONTENT_REG);    //  0 0 C3 C2 C1 C0 D9 D8 
    i2c_periph_write(SET_C2 | SET_C1 | SET_C0);    //  0 0 C3 C2 C1 C0 D9 D8
    i2c_periph_write(SET_D1 | SET_D0);    //  D7 D6 D5 D4 D3 D2 D1 D0
    
    i2c_periph_stop();

    // read register content
    i2c_periph_start(AD5274_addr | READ_REG);
    i2c_periph_write(SET_C2 | SET_C1 | SET_C0);    //  0 0 C3 C2 C1 C0 D9 D8

    status[1] = i2c_read_ack(100,NULL);      //  D7 D6 D5 D4 D3 D2 D1 D0
    status[0] = i2c_read_nack(100,NULL);      //  D7 D6 D5 D4 D3 D2 D1 D0
    
    i2c_periph_stop();
                   
    printf("--> Controll Reg 0x%x 0x%x\n\r",status[1],status[0]);

    if( status[0] & SET_D0 & SET_D1)
        return 0;
    else
        return -16;
        
    
}
#endif
