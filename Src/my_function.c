#include "my_function.h"


// hitung nilai current source
float data_to_current_value(unsigned char data){
    float R_wa_full = 20000.0;
    float R_wa_d;
    float I_out;  
    float data_float = data;
    
    R_wa_d = (data_float/256.0)*R_wa_full;
    //printf("data %d R_wa_d %1.3f ohm\n\r ",data,R_wa_d);
    I_out = (0.00001*R_wa_d)/4.99;
    
    return I_out;                               
}

// hitung nilai current source
unsigned char current_to_data_value(float *arus){
    float R_wa_full = 20000.0;
    float R_wa_d;
    float I_out;  
    float data_float;
    unsigned char data;       
    
    // find R_wa_d                       
    R_wa_d=(*arus*4.99)/0.00001;
    //printf("R_wa_d %f ohm\n\r ",R_wa_d);
    // find data_float
    data_float = (R_wa_d/R_wa_full)*256.0;                           
    //printf("data_float %f ohm\n\r ",data_float);
    data = data_float;       
    //printf("data %d ohm\n\r ",data);
    return data;                               
}

int cek_controll_reg_ds(unsigned char data){

        int error_value;
        #define BIT_6 1<<6 
        #define BIT_EOSC 1<<7   
        #define BIT_BBSQI 1<<5  
        #define BIT_INTCN 1<<2 
                       
        if(data & BIT_6)
        {
            // bit 6 must be 0
            error_value = CNTRL_REG_FAIL;
        }else
        {
            error_value = 0;

            if(data & BIT_EOSC){
                // this data must be 0 if 1 then Oscilator problem
                error_value = EOSC_FAIL;
            }
            else{}
            if(data & BIT_BBSQI){}
            else
            {
                // this data must be 1 if 0 then interrupt disable
                error_value = BBSQI_FAIL;
            }
            if(data & BIT_INTCN){}
            else{
                // this data must be 1 if 0 then interrupt disable
                error_value = INTCN_FAIL;
            }        
        }            
        return error_value;
}

//            // OSF nya kok 1 terus ya?
//            status_rtc = cek_status_reg_ds(Status_Reg); 
//            //printf(">>> status_rtc %d\n\r",status_rtc);                                      
//             if(status_rtc != SUCCESS){
//                if(status_rtc == -1){
//                    // Status Register Data Not Valid, because bit 6 is 1
//                    glcd_printfxy(0,50,"E005E"); 
//                }
//                else if(status_rtc == -2){          
//                    // Oscilator Fail
//                    glcd_printfxy(0,50,"E005F");
//                }
//                else if(status_rtc == -3){
//                    // Alarm is not raise
//                    glcd_printfxy(0,50,"E005G"); 
//                }                               
//                delay_ms(2000);
//                goto START;
//            } 
int cek_status_reg_ds(unsigned char data){

        int error_value;
        #define BIT_OSF 1<<7 
        #define BIT_A1F 1<<0 
                       
        // pastikan data bit 6 - 2 itu 0
        // jika tidak invalid status register
        if(data & 1<<6 && data & 1<<5 && data & 1<<4 && data & 1<<3 && data & 1<<2)
        {
            error_value = -1;
        }else
        {
            error_value = 0;

            if(data & BIT_OSF){
                error_value = -2;
            }
            else{}
            if(data & BIT_A1F){}
            else
            {
                error_value = -3;
            }        
        }            
        return error_value;
}


/**
 * [set_alarm --> Set Alarm berikutnya]
 * @param  rentang_menit [interval menit alarm berikutnya]
 */
int set_alarm(unsigned int rentang_menit){
    unsigned int next_menit;           
    unsigned char set_detik,set_menit,set_jam;
               
    // test MOSFET 
    //#define TEST_MOSFET 
    // force 10 detik
    //#define DEBUG_ALARM
    
    #ifdef DEBUG_ALARM
        printf(">>> Setting Alarm\n\r");
    #endif
    
    if(ds1339_get_time(&set_jam,&set_menit,&set_detik)!=SUCCESS)
        return GET_TIME_FOR_ALARM_FAIL; 

    #ifdef DEBUG_ALARM
        printf("Time Now = %d:%d:%d \n\r",set_jam,set_menit,set_detik);
    #endif
                             
    #ifndef TEST_MOSFET
        next_menit=set_menit;     
        next_menit +=rentang_menit;
             
        set_jam=set_jam+(next_menit/60);
              
        set_menit=next_menit%60;
        set_jam=set_jam%24;
    #else
        set_detik=set_detik+5;    
    #endif
   
     
    #ifdef DEBUG_ALARM
        printf("Next Alarm = %d:%d:%d \n\r",set_jam,set_menit,set_detik);
    #endif
    
    ds1339_set_alrm1(set_jam,set_menit,set_detik);                       
    
    if(ds1339_get_alrm1(&set_jam,&set_menit,&set_detik)!=SUCCESS)
        return GET_ALRM_FAIL; 

    #ifdef DEBUG_ALARM
        printf("Cek Alarm = %d:%d:%d \n\r",set_jam,set_menit,set_detik);
        printf(">>> Setting Alarm (Done)\n\r");
    #endif
                     
    #undef DEBUG_ALARM   
    
    return 0;
}

/**
 * [rtc_to_mem --> Save Time Stamp]
 * @param  addr [lokasi memory]
 */
int rtc_to_mem(unsigned long addr)
{                         
    // unsigned char t;
    unsigned char set_detik,set_menit,set_jam;
    unsigned char set_tanggal,set_bulan,set_tahun;    
    unsigned char baca_men,baca_jam; 
    unsigned char coba;
    unsigned char bcd_men;
    unsigned char bcd_jam;
    unsigned char bcd_tgl;
    unsigned char bcd_bln;
    
    
    if(ds1339_get_time(&set_jam,&set_menit,&set_detik)!=SUCCESS){
        return GET_TIME_FOR_STAMP_FAIL;                                         
    }
                  

ULANG_WRITE:              
    printf("Save Time Stamp...\n\r");     

    // kadang nilainya ngaco di cek lagi
    bcd_men = bin2bcd(set_menit);
    bcd_jam = bin2bcd(set_jam);
    
    EEWrite(addr, (uint8_t *)&bcd_men,1);
    EEWrite(addr+1, (uint8_t *)&bcd_jam,1);    

    EERead (addr,(uint8_t *)&baca_men,1);
    EERead (addr+1,(uint8_t *)&baca_jam,1);
    baca_men = bcd2bin(baca_men);
    baca_jam = bcd2bin(baca_jam);   
    
    if(baca_men != set_menit ||
        baca_jam != set_jam
    ){                 
        coba++;
        if(coba>=5)return EEPROM_ERROR;
        else {
            printf("Time Stamp Verify Fail!\n\r");     
            goto ULANG_WRITE;
        }
    }
                    
    if(ds1339_get_date(&set_tanggal,&set_bulan,&set_tahun)!=SUCCESS){
        return GET_DATE_FOR_STAMP_FAIL;
    }
                                                  
    bcd_tgl = bin2bcd(set_tanggal);
    bcd_bln = bin2bcd(set_bulan);
    
    EEWrite (addr+2, (uint8_t *)&bcd_tgl,1);
    EEWrite (addr+3, (uint8_t *)&bcd_bln,1);
    EEWrite (addr+4, (uint8_t *)&set_tahun,1);    
                
    // eeprom1025_write_array(addr, date_time,  5);   
    // eeprom1025_read_array(date_time, addr, 5) ;
        
    // menit   = bcd2bin(date_time[0]);
    // jam     = bcd2bin(date_time[1]);
    // tanggal = bcd2bin(date_time[2]);
    // bulan   = bcd2bin(date_time[3]);
    // tahun   = date_time[4] ;    
    // printf("%d/%d/%d %d:%d \n\r",tanggal,bulan,tahun,jam,menit);     
                  
    printf("Berhasil.\n\r");     

    return 0;
}

/**
 * [read_time_mem --> Read start time]
 * @param  addr [lokasi memory]
 */
struct tm read_time_mem(long addr)
{
    unsigned char date_time[6]={0};
    unsigned char t;
    unsigned char set_detik,set_menit,set_jam;
    unsigned char set_tanggal,set_bulan,set_tahun;
    struct tm set_waktu;

    for(t=0;t<=4;t++){
        EERead(addr+t,(uint8_t *)&date_time[t],1);  
    }              
    //jam menit
    set_menit    =(((date_time[0]&0xF0)>>4)*10)+(date_time[0]&0x0F);
    set_jam      =(((date_time[1]&0x30)>>4)*10)+(date_time[1]&0x0F);
    set_tanggal  =(((date_time[2]&0x30)>>4)*10)+(date_time[2]&0x0F);
    set_bulan    =(((date_time[3]&0x10)>>4)*10)+(date_time[3]&0x0F);
    set_tahun=date_time[4];  
    
    set_waktu.hour = set_jam;
    set_waktu.minute = set_menit;
    set_waktu.day = set_tanggal;
    set_waktu.month = set_bulan;
    set_waktu.year = set_tahun;
    
    return set_waktu;    
}

// ?
void pot_write(unsigned char pot) {
    unsigned char addr;
    addr=0x58;

    i2c_periph_start(addr);
    i2c_periph_write(0);
    i2c_periph_write(pot);
    i2c_periph_stop();
    delay_ms(10);
}

#if 0
int i2c_scanner(void){
    unsigned char address;

    int nDevices =0;
    unsigned char acknowledge_detect;

    printf("=====> Scanning...\n\r");

    nDevices = 0;
    for(address = 1; address < 127; address++ ) 
    {
        );   
        acknowledge_detect = i2c_periph_write(address);
        if(acknowledge_detect)
        {
            printf("=====> Found Address : 0x%x\n\r",address);
            nDevices++;
        }
        i2c_periph_stop();   
                   
        delay_ms(10);     
    }    
    printf("=====> Found : %d Device\n\r",nDevices);
    return nDevices;    
}
#endif


/**
 * [test_rw_eeprom --> Test EEPROM]
 */
int test_rw_eeprom(void){

    //#define DEBUG_TEST_EEPROM
    
    // after addr wdg counter
    #define ADDR_TEST 60003      
    #define LEN_TEST 3

    unsigned long a; 
    int b;
    int status;                
    unsigned char write_value[LEN_TEST];
    unsigned char read_value[LEN_TEST];

    // test write
    b =0;
    for (a=ADDR_TEST;a<ADDR_TEST+LEN_TEST;a++)
    {
        write_value[b] = b;
//        printf("write ee\n\r");
        EEWrite(a,(uint8_t *)&write_value[b],1);
//        printf("write ee done\n\r");
        b++;
    }
    // test read
    b =0;
    for (a=ADDR_TEST;a<ADDR_TEST+LEN_TEST;a++)
    {                        
//        printf("read ee\n\r");
         EERead(a,(uint8_t *)&read_value[b],1) ;
//        printf("read ee done\n\r");
         b++;         
    }     
    // compare value
    for (b=0;b<LEN_TEST;b++)
    {
        if(read_value[b] != write_value[b]){
            #ifdef DEBUG_TEST_EEPROM
                printf("read[%d]=0x%x not equal write[%d]=0x%x\n\r",b,read_value[b],b,write_value[b]);
            #endif
            status = -1;
        }               
        else{
            #ifdef DEBUG_TEST_EEPROM
                printf("read[%d]=0x%x equal write[%d]=0x%x\n\r",b,read_value[b],b,write_value[b]);
            #endif
            status = 0;
        }
    }     
    return status;            
}

void dump_data(unsigned char *cek_data,unsigned long addr_data){                        
    int a; 
    for(a = 0;a<11;a++){
        printf("ADDR %d DATA 0x%x",addr_data+a,cek_data[a]);
    }    
}    

/**
 * [nyari_read_interval --> Hitung interval waktu baca sensor dari eeprom]
 * @param  none
 * @return 
 */
unsigned int nyari_read_interval(void){
    unsigned char jam_temp;
    unsigned char jam_i,jam_pul,jam_sat,men_pul,men_sat;
    unsigned int men_i;
    unsigned char tanggal_i; 
    unsigned int interval_minute;

    #ifdef DEBUG_EEPROM
        printf(">>> Nyari Read Interval\n\r");
    #endif

    tanggal_i=0;
    EERead(16,&jam_pul,1);
    EERead(17,&jam_sat,1);
    
    jam_i=((jam_pul-'0')*10)+(jam_sat-'0');

    EERead(18,&men_pul,1);
    EERead(19,&men_sat,1);
    men_i=((men_pul-'0')*10)+(men_sat-'0');
         
    #ifdef DEBUG_EEPROM
        printf(">>> Cek EEPROM Contain\n\r");
        printf(">>> addr 16 = %d \n\r",eeprom1025_read(16));
        printf(">>> addr 17 = %d \n\r",eeprom1025_read(17));
        printf(">>> addr 18 = %d \n\r",eeprom1025_read(18));
        printf(">>> addr 19 = %d \n\r",eeprom1025_read(19));
    #endif

    jam_temp=jam_i;
            
    if (jam_temp>23){
        jam_i=0;
        tanggal_i=1;
    }
    
    #ifdef DEBUG_EEPROM
        printf("--> %d hari %d jam %d menit\n\r",tanggal_i,jam_i,men_i);
    #endif
                          
    // tanggal_i = hari
    // 1 hari = 24*60 = 1440 menit
    // 1 jam = 60 menit
    interval_minute=(tanggal_i*1440)+(jam_i*60)+men_i;
    return interval_minute;
}

/************************************************************************************************
 * Format RW EEPROM
 ************************************************************************************************/          

// format config start in addr 0 in eeprom
// 25 byte
// Probetype	TAG															READ INTERVAL	START TIME				
// x1          x1	x2	x3	x4	x5	x6	x7	x8	x9	x10	x11	x12	x13	x14	x15	x1	x2	x3	x4	x1	x2	x3	x4	x5
uint8_t write_data_config_to_eeprom(uint16_t addr,unsigned char val){
    uint8_t status;
    status = EEWrite(addr,&val,1);
    return status;
}

// lokasi alamat nilai jumlah data di setelah data_config
// baca jumlah_data
// jumlah data --> 16 bit
// eeprom hanya 8 bit     
// 256 --> 1 0000 0000   
uint8_t read_total_data_from_eeprom(unsigned int *total){    
    uint8_t status;
    unsigned char temp_value;
    unsigned int value;
    
    status = EERead(25,&temp_value,1);
           
    if(status != 0)
        return status;
        
    value=temp_value*256;
    status = EERead(26,&temp_value,1);
    
    value=value+temp_value;  
                 
    *total = value;
    return status;
}

uint8_t write_total_data_to_eeprom(unsigned int val){
    uint8_t status;
    unsigned char temp_value;
                   
    temp_value=val>>8;
    status = EEWrite(25,&temp_value,1);   
    if(status != 0)
        return status;
    temp_value=val & 0xFF;
    status = EEWrite(26,&temp_value,1);
    if(status != 0)
        return status;
}
                                                
// untuk address penyimpanan di geser offset nya ke 201
// untuk spare, semisal mw nambahin setting config nya
// setiap ambil data menyimpan 11 byte data
// <-------------------------- 11 byte ---------------------------->                        
// menit    jam     tanggal    bulan    tahun    ML/CR            CEK/temp    
// 0        1       2       3       4       5   6    7    8    9    10
// <------------ 5 byte ------------>
    
uint8_t read_metalloss_from_eeprom(float *ml,unsigned int offset){
    uint8_t status;
    float ml_val;      
    unsigned char balik[4];                                           
    typedef union
    {
        float          a;
        unsigned char  b[sizeof(float)];
    } c ;
    c d;

    //status = EERead(201+((offset-1)*11)+5,(uint8_t *)&balik[0],sizeof(float));
    status = EERead(201+((offset-1)*11)+5,(uint8_t *)&balik[0],1);
    status += EERead(201+((offset-1)*11)+6,(uint8_t *)&balik[1],1);
    status += EERead(201+((offset-1)*11)+7,(uint8_t *)&balik[2],1);
    status += EERead(201+((offset-1)*11)+8,(uint8_t *)&balik[3],1);
            
    if(status != 0)return EEPROM_ERROR;  
    
//    printf("status = %d\r\n",status);  
    
    d.b[3]=balik[0];
    d.b[2]=balik[1];
    d.b[1]=balik[2];
    d.b[0]=balik[3];
    *ml = d.a;
    return status;
}

uint8_t write_metalloss_to_eeprom(float ml,unsigned int offset){
    uint8_t status;     
    unsigned char balik[4];                                           
    unsigned char compare[4];                                           
    typedef union
    {
        float          a;
        unsigned char  b[sizeof(float)];
    } c ;
    c d;
                                    
    d.a=ml;
    
    balik[0]=d.b[3];
    balik[1]=d.b[2];
    balik[2]=d.b[1];
    balik[3]=d.b[0];

ULANG_WR_EEPROM:
    //status = EEWrite(201+(offset*11)+5,(uint8_t *)&balik[0],sizeof(float));
    status = EEWrite(201+(offset*11)+5,(uint8_t *)&balik[0],1);
    status = EEWrite(201+(offset*11)+6,(uint8_t *)&balik[1],1);
    status = EEWrite(201+(offset*11)+7,(uint8_t *)&balik[2],1);
    status = EEWrite(201+(offset*11)+8,(uint8_t *)&balik[3],1);
    if(status != 0)
        return -1;

    //status = EERead(201+(offset*11)+5,(uint8_t *)&compare[0],sizeof(float));
    status = EERead(201+(offset*11)+5,(uint8_t *)&compare[0],1);
    status = EERead(201+(offset*11)+6,(uint8_t *)&compare[1],1);
    status = EERead(201+(offset*11)+7,(uint8_t *)&compare[2],1);
    status = EERead(201+(offset*11)+8,(uint8_t *)&compare[3],1);
    if(status != 0)
        return -1;  
        
    d.b[3]=compare[0];
    d.b[2]=compare[1];
    d.b[1]=compare[2];
    d.b[0]=compare[3];

//    if(d.a != ml){
    if(compare[0] != balik[0] \
    || compare[1] != balik[1] \
    || compare[2] != balik[2] \
    || compare[3] != balik[3] \
    ){
        //printf("ULANGI WR EEPROM!!!!!!!!!!!!!!! da = %f ml = %f\r\n",d.a,ml); 
        printf("compare[0] 0x%x 0x%x\r\n",compare[0],balik[0]); 
        printf("compare[1] 0x%x 0x%x\r\n",compare[1],balik[1]); 
        printf("compare[2] 0x%x 0x%x\r\n",compare[2],balik[2]); 
        printf("compare[3] 0x%x 0x%x\r\n",compare[3],balik[3]); 
        delay_ms(100); 
      goto ULANG_WR_EEPROM;
    }
    return status;
}

uint8_t read_byte_calculation(unsigned int offset,unsigned char bite,unsigned char *value){
    uint8_t status;
    status = EERead(201+(offset*11)+bite,value,1);   
    return status;
}

uint8_t write_pv_rcek_to_eeprom(unsigned int offset,unsigned int res)
{
    unsigned char temp_value;
    uint8_t status;
    temp_value=res>>8; 
    status = EEWrite(201+(offset*11)+9,&temp_value,1);
    if(status != 0)
        return status;
        
    temp_value=res & 0xFF;      
    status = EEWrite(201+(offset*11)+10,&temp_value,1); 

    return status;
    
}

// lokasi alamat nilai corrate di antara data_config dan data_measurement
uint8_t read_corrate_from_eeprom(float *value){
    uint8_t status;
    status = EERead(37,(uint8_t *)value,sizeof(float));
    return status;
}

uint8_t write_corrate_to_eeprom(float value){
    uint8_t status;
    EEWrite( 37, (uint8_t *)&value,sizeof(float));
    return status;
}

// probe type in address 0
uint8_t read_probe_type(unsigned char *tipe){
    uint8_t status;
    status = EERead(0,tipe,1);
    return status;
}
                      