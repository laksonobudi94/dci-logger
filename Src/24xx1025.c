/*
  EEPROM_24X1025.c - Library for interfacing 24XX1025 EEPROM chips.
 */

#include "24xx1025.h"

// uncomment for debug
//#define DEBUG_EEPROM

/**
 * [eeprom1025_read --> baca data dari eeprom]
 * @param  address [alamat data]
 * @return data            [data dari eeprom]
 */
unsigned char eeprom1025_read(unsigned long address) {

  unsigned char data;
  unsigned char addr;

  #ifdef DEBUG_EEPROM
    printf("BACA EEPROM \n\r");
  #endif

  addr=EEPROM24_ADDR;

  // buat 2 ic
  // 1010           B0                A1 A0             R/~W
  // Controll Code  Block Select Bit  Chip Select Bit
  if(address>65535){
      addr=0xA6;
      address-=0xFFFF;
  }         
  #ifndef USE_TWI
      i2c_start();
  #endif


  #ifdef DEBUG_EEPROM
    printf("I2C Started \n\r",data);
  #endif

  #ifndef USE_TWI
    i2c_write(addr);
  #else
    i2c_start(addr);
  #endif
  
  #ifdef DEBUG_EEPROM
    printf("I2C Written \n\r",data);
  #endif

  // Address High Byte
  i2c_write(address>>8);
  // Address Low Byte
  i2c_write((unsigned char)address);

  #ifndef USE_TWI
      i2c_start();
  #endif

  #ifndef USE_TWI
    i2c_write(addr | READ_CMD);
  #else
    i2c_start(addr | READ_CMD);
  #endif

  #ifndef USE_TWI
      data=i2c_read(0);     
  #else
      data=i2c_read_nack();
  #endif

  i2c_stop();

  #ifdef DEBUG_EEPROM
    printf("DATA = %d \n\r",data);
  #endif         
  
   //printf("Read addr %lu Data 0x%x \n\r",address,data);


  return data;
}

/**
 * [eeprom1025_write --> tulis data ke eeprom]
 * @param  address [alamat eeprom]
 * @param  data        [data eeprom]
 */
void eeprom1025_write(unsigned long address, unsigned char data) {
  unsigned char addr;

  //printf("Send addr %lu Data 0x%x \n\r",address,data);


  #ifdef DEBUG_EEPROM
    printf("TULIS EEPROM \n\r");
  #endif

  addr=EEPROM24_ADDR;
  if(address>65535){
      addr=0xA6;
      address-=0xFFFF;
  }
          
  #ifndef USE_TWI
      i2c_start();
  #endif


  #ifdef DEBUG_EEPROM
    printf("I2C Started \n\r",data);
  #endif

  #ifndef USE_TWI
    i2c_write(addr);
  #else
    i2c_start(addr);
  #endif
  
  i2c_write(address>>8);
  i2c_write((unsigned char)address);
  i2c_write(data);
  i2c_stop();
  delay_ms(10);

  #ifdef DEBUG_EEPROM
    printf("I2C Written \n\r",data);
  #endif

}

/**
 * [eeprom1025_write_float --> tulis data float ke eeprom]
 * @param  address      [alamat eeprom]
 * @param  data         [data float dari eeprom]
 */
void eeprom1025_write_float(unsigned long address, float data) {
  unsigned char addr;
  typedef union
  {
    float          a;
    unsigned char  b[sizeof(float)];
  } c ;
  c d;

  #ifdef DEBUG_EEPROM
    printf("TULIS DATA FLOAT KE EEPROM\n\r");
  #endif

  addr = EEPROM24_ADDR;

  // for 2 ic
  if(address>65535){
    addr=0xA6;
    address-=0xFFFF;
  }

  d.a=data; 

  i2c_start();

  #ifdef DEBUG_EEPROM
    printf("I2C Started \n\r",data);
  #endif

  // FIGURE 6-2: PAGE WRITE
  i2c_write(addr);
  // address high byte
  i2c_write(address>>8);
  // address low byte
  i2c_write((unsigned char)address);
  i2c_write(d.b[3]);
  i2c_write(d.b[2]);
  i2c_write(d.b[1]);
  i2c_write(d.b[0]);
  i2c_stop();
  delay_ms(10);

  #ifdef DEBUG_EEPROM
    printf("I2C Written \n\r",data);
  #endif
}

/**
 * [eeprom1025_read_float --> baca data float dari eeprom]
 * @param  address [alamat data]
 * @return data            [data float dari eeprom]
 */
float eeprom1025_read_float(unsigned long address) {
  float data;
  unsigned char addr;
  typedef union
    {
        float          a;
        unsigned char  b[sizeof(float)];
    } c ;
  c d;

  addr=EEPROM24_ADDR;

  // for 2 ic
  if(address>65535){
      addr=0xA6;
      address-=0xFFFF;
  }

  // PAGE READ
  i2c_start();
  i2c_write(addr);
  // address high byte
  i2c_write(address>>8);
  // address low byte
  i2c_write((unsigned char)address);
  i2c_start();
  i2c_write(addr | READ_CMD);
  d.b[3]=i2c_read(1);
  d.b[2]=i2c_read(1);
  d.b[1]=i2c_read(1);
  d.b[0]=i2c_read(0);
  i2c_stop();
  data=d.a; 
  return data;
}

/**
 * [nyari_read_interval --> Hitung interval waktu baca sensor dari eeprom]
 * @param  none
 * @return 
 */
unsigned int nyari_read_interval(void){
    unsigned char jam_temp;
    unsigned char jam_i;
    unsigned int men_i;
    unsigned char tanggal_i; 
    unsigned int interval_minute;

    #ifdef DEBUG_EEPROM
        printf(">>> Nyari Read Interval\n\r");
    #endif

    tanggal_i=0;
    jam_i=((eeprom1025_read(16)-'0')*10)+((eeprom1025_read(17)-'0'));
    men_i=((eeprom1025_read(18)-'0')*10)+((eeprom1025_read(19)-'0'));
         
    #ifdef DEBUG_EEPROM
        printf(">>> Cek EEPROM Contain\n\r");
        printf(">>> addr 16 = %d \n\r",eeprom1025_read(16));
        printf(">>> addr 17 = %d \n\r",eeprom1025_read(17));
        printf(">>> addr 18 = %d \n\r",eeprom1025_read(18));
        printf(">>> addr 19 = %d \n\r",eeprom1025_read(19));
    #endif

    jam_temp=jam_i;
            
    if (jam_temp>23){
        jam_i=0;
        tanggal_i=1;
    }
    
    #ifdef DEBUG_EEPROM
        printf("--> %d hari %d jam %d menit\n\r",tanggal_i,jam_i,men_i);
    #endif
                          
    // tanggal_i = hari
    // 1 hari = 24*60 = 1440 menit
    // 1 jam = 60 menit
    interval_minute=(tanggal_i*1440)+(jam_i*60)+men_i;
    return interval_minute;
}
