#include "ds1339_twi.h"
#include <stdio.h>

/**
 * [ds1339_get_time --> baca waktu dari rtc]
 * @param  hour [jam]
 * @param  min [menit]
 * @param  sec [detik] 
 * @return status 0 = Ok, -1 = fail
 */
int ds1339_get_time(unsigned char *hour,unsigned char *min,unsigned char *sec)
{                
    uint8_t no_response_from_ic;
    
    // Please Read:
    // Figure 9. Data Read (Write Pointer, Then Read)—Slave Receive and Transmit
    i2c_periph_start(ADDR_DS1399);         
    i2c_periph_write(ADDR_SECONDS);  
    i2c_periph_start(ADDR_DS1399 | READ_CMD);

    *sec=bcd2bin(i2c_read_ack(100,&no_response_from_ic));
    if(no_response_from_ic)
        return -1;    
    *min=bcd2bin(i2c_read_ack(100,&no_response_from_ic));
    if(no_response_from_ic)
        return -1;
    *hour=bcd2bin(i2c_read_nack(100,&no_response_from_ic)); 
    if(no_response_from_ic)
        return -1;
             
    if(*sec < 0 || *sec > 59 || 
        *min < 0 || *min > 59 || 
        *hour < 0 || *hour > 23)
    {                 
        printf("time error: %d:%d:%d",*hour,*min,*sec);
        return -1;
    }

    i2c_periph_stop();      
    
    return 0;
}

/**
 * [ds1339_get_date --> baca tanggal dari rtc]
 * @param  day [tanggal]
 * @param  month [bulan]
 * @param  year [tahun]
 * @return status 0 = Ok, -1 = fail
 */
int ds1339_get_date(unsigned char *day,unsigned char *month,unsigned char *year)
{                               
    uint8_t no_response_from_ic;

    i2c_periph_start(ADDR_DS1399);         
    i2c_periph_write(ADDR_DAY);  
    i2c_periph_start(ADDR_DS1399 | READ_CMD);
    *day=bcd2bin(i2c_read_ack(100,&no_response_from_ic));       
    if(no_response_from_ic)
        return -1; 
    *month=bcd2bin(i2c_read_ack(100,&no_response_from_ic));
    if(no_response_from_ic)
        return -1; 
    *year=bcd2bin(i2c_read_nack(100,&no_response_from_ic)); 
    
    if(*day < 1 || *day >31 || 
        *month < 1 || *month > 12 || 
        *year < 0 || *year > 99)
    {
        return -1;
    }
     
    i2c_periph_stop();

    return 0;
}

/**
 * [ds1339_set_time --> setting waktu ke rtc]
 * @param  hour [tanggal]
 * @param  min [bulan]
 * @param  sec [tahun]
 */
void ds1339_set_time(unsigned char hour,unsigned char min,unsigned char sec)
{
    // Please Read:
    // Figure 7. Data Write—Slave Receiver Mode
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(ADDR_SECONDS);
    i2c_periph_write(bin2bcd(sec));
    i2c_periph_write(bin2bcd(min));
    i2c_periph_write(bin2bcd(hour)); 
    i2c_periph_stop();
    delay_ms(10);      
}

/**
 * [ds1339_set_date --> setting tanggal ke rtc]
 * @param  day [tanggal]
 * @param  month [bulan]
 * @param  year [tahun]
 */
void ds1339_set_date(unsigned char day,unsigned char month,unsigned char year)
{  
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(ADDR_DAY);
    i2c_periph_write(bin2bcd(day));
    i2c_periph_write(bin2bcd(month));
    i2c_periph_write(bin2bcd(year)); 
    i2c_periph_stop();
    delay_ms(10);
}

/**
 * [ds1339_set_alrm1 --> setting alarm ke rtc]
 * @param  hour [jam]
 * @param  min [menit]
 * @param  sec [detik]
 */
void ds1339_set_alrm1(unsigned char hour,unsigned char min,unsigned char sec)
{
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(ADDR_ALARM1_SECONDS);
    i2c_periph_write(bin2bcd(sec));
    i2c_periph_write(bin2bcd(min));
    i2c_periph_write(bin2bcd(hour)); 

    // Please Read:
    // Table 4. Alarm Mask Bits 
    // Alarm when hours, minutes, and seconds match
    i2c_periph_write(SET_A1M4); 
    i2c_periph_stop();
    delay_ms(10);

}

/**
 * [ds1339_set_alrm1 --> setting alarm ke rtc]
 * @param  hour [jam]
 * @param  min [menit]
 * @param  sec [detik]
 */
int ds1339_get_alrm1(unsigned char *hour,unsigned char *min,unsigned char *sec)
{
    i2c_periph_start(ADDR_DS1399);         
    i2c_periph_write(ADDR_ALARM1_SECONDS);  
    i2c_periph_start(ADDR_DS1399 | READ_CMD);

    *sec=bcd2bin(i2c_read_ack(100,NULL));
    *min=bcd2bin(i2c_read_ack(100,NULL));
    *hour=bcd2bin(i2c_read_nack(100,NULL));
    
    if(*sec < 0 || *sec > 59 || 
        *min < 0 || *min > 59 || 
        *hour < 0 || *hour > 23)
    {                        
        printf("alarm error: %d:%d:%d",*hour,*min,*sec);
        return -1;
    }     
     
    i2c_periph_stop();          
    
    return 0;
} 

/**
 * [ds1339_set_alrm2 --> setting alarm ke rtc]
 * @param  hour [jam]
 * @param  min [menit]
 */
void ds1339_set_alrm2(unsigned char hour,unsigned char min)
{
    unsigned char bcd_min,bcd_hour;
                          
    bcd_min = bin2bcd(min);
    bcd_hour = bin2bcd(hour);    
    
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(ADDR_ALARM2_MINUTES);
    i2c_periph_write(bcd_min);
    i2c_periph_write(bcd_hour);
    // Alarm when hours and minutes match 
    i2c_periph_write(SET_A2M4); 
    i2c_periph_stop();
    delay_ms(10);   
   
   
}

/**
 * [ds1339_get_alrm2 --> baca alarm dari rtc]
 * @param  hour [jam]
 * @param  min [menit]
 */
int ds1339_get_alrm2(unsigned char *hour,unsigned char *min)
{
    i2c_periph_start(ADDR_DS1399);         
    i2c_periph_write(ADDR_ALARM2_MINUTES);  
    i2c_periph_start(ADDR_DS1399 | READ_CMD);

    *min = i2c_read_ack(100,NULL);
    *hour = i2c_read_nack(100,NULL);
    
    *min=bcd2bin(*min);
    *hour=bcd2bin(*hour); 

    if(*min < 0 || *min > 59 || 
        *hour < 0 || *hour > 23)
    {
        return -1;
    }     
    i2c_periph_stop();   
    
    return 0;
}  

/**
 * [ds1339_get_status --> cek status register]
 * @param  statE [CONTROL REGISTER]
 * @param  statF [STATUS REGISTER]
 */
int ds1339_get_status(unsigned char *statE,unsigned char *statF)
{
    uint8_t no_response_from_ic;
    
    i2c_periph_start(ADDR_DS1399);         
    i2c_periph_write(CONTROL_REGISTER);  
    i2c_periph_start(ADDR_DS1399 | READ_CMD);

    // CONTROL REGISTER
    *statE=i2c_read_ack(100,&no_response_from_ic);
     if(no_response_from_ic)
        return -1;     
    
    
    // STATUS REGISTER
    *statF=i2c_read_nack(100,&no_response_from_ic); 
     if(no_response_from_ic)
        return -1;     
     
    i2c_periph_stop();     
    
    return 0; 
}

/**
 * [ds1339_init --> inisialisasi RTC]
 */
void ds1339_init(void)
{
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(TRICKLE_CHARGER_REGISTER);
    // Initial power-up values
    i2c_periph_write(0x00);
    i2c_periph_stop();
    delay_ms(10);    
   
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(CONTROL_REGISTER);
    
    // SET_BBSQI
    // enables the
    // square wave or interrupt output when VCC is absent
    // and the DS1339 is being powered by the VBACKUP pin
    
    // SET_INTCN
    // a match between the timekeeping registers
    // and the alarm 1 or alarm 2
    // registers activate the SQW/INT pin (provided that the alarm is enabled).

    // SET_A1IE
    // permits the Alarm 1 Flag (A1F) bit in the
    // status register to assert SQW/INT (when INTCN = 1).
    i2c_periph_write(SET_BBSQI | SET_INTCN | SET_A1IE);

    i2c_periph_stop();
    delay_ms(10);   
}

/**
 * [ds1339_alarm1_on --> aktifkan alarm 1]
 */
void ds1339_alarm1_on(void)
{
    
    i2c_periph_start(ADDR_DS1399);
    i2c_periph_write(CONTROL_REGISTER); 
    i2c_periph_write(SET_BBSQI | SET_INTCN | SET_A1IE);
    i2c_periph_stop();     
    delay_ms(10); 
   
    i2c_periph_start(ADDR_DS1399); 
    i2c_periph_write(STATUS_REGISTER); 

    // clear status register
    i2c_periph_write(0x00);    
    i2c_periph_stop();        
    delay_ms(10);    
}

/**
 * [ds1339_alarm2_on --> aktifkan alarm 2]
 */
void ds1339_alarm2_on(void)
{
i2c_periph_start(ADDR_DS1399);
i2c_periph_write(CONTROL_REGISTER);
i2c_periph_write(SET_BBSQI | SET_INTCN | SET_A2IE);
i2c_periph_stop();
delay_ms(10);        
}

