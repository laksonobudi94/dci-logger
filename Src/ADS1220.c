//////////////////////////////////////////////////////////////////////////////////////////
//
//    Arduino library for the ADS1220 24-bit ADC breakout board
//
//    Author: Ashwin Whitchurch
//    Copyright (c) 2018 ProtoCentral
//
//    Based on original code from Texas Instruments
//
//    This software is licensed under the MIT License(http://opensource.org/licenses/MIT).
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
//   NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//   For information on how to use, visit https://github.com/Protocentral/Protocentral_ADS1220/
//
/////////////////////////////////////////////////////////////////////////////////////////

#include <ADS1220.h>

#define adc_drdy PINB.2
#define adc_cs PORTB.3

//#define DEBUG_ADS1220

#define _BV(n) (1 << n)   


#define PGA          1                 // Programmable Gain = 1
#define VREF         3.300            // Internal reference of 3.300V
#define VFSR         VREF/PGA
#define FULL_SCALE   (((long int)1<<23)-1)
             
uint8_t m_config_reg0;
uint8_t m_config_reg1;
uint8_t m_config_reg2;
uint8_t m_config_reg3;

uint8_t Config_Reg0;
uint8_t Config_Reg1;
uint8_t Config_Reg2;
uint8_t Config_Reg3;
uint8_t NewDataAvailable;
void ADS1220_writeRegister(unsigned char address, unsigned char value)
{
      adc_cs=0;

      delay_ms(5);
      /*
      WREG (0100 rrnn)

      The WREG command writes the number of bytes specified by nn (number of bytes to be written – 1) to the
      device configuration register, starting at register address rr. The command is completed after nn + 1 bytes are
      clocked in after the WREG command byte. For example, the command to write two bytes (nn = 01) starting at
      configuration register 0 (rr = 00) is 0100 0001. 
      */
      spi(WREG|(address<<2));
      spi(value);
      delay_ms(5);

      adc_cs=1;
}

uint8_t ADS1220_readRegister(uint8_t address)
{
      uint8_t data;

      adc_cs=0;
      delay_ms(5);

      /*
      RREG (0010 rrnn)

      The RREG command reads the number of bytes specified by nn (number of bytes to be read – 1) from the
      device configuration register, starting at register address rr. The command is completed after nn + 1 bytes are
      clocked out after the RREG command byte.
      */
      spi(RREG|(address<<2));
      data = spi(SPI_MASTER_DUMMY);
      delay_ms(5);
      adc_cs=1;

      return data;
}

void ADS1220_begin()
{
    delay_ms(100);   
                  
    #ifdef DEBUG_ADS1220
        printf("ADS1220_Reset\r\n");
    #endif                        
                  
    ADS1220_Reset();
    
    #ifdef DEBUG_ADS1220
        printf(" Done\r\n");
    #endif

    delay_ms(100);

    adc_cs=0;

    // ads1220_init(0xB0,0x00,0xE0,0x00 );

    m_config_reg0 = 0xB0;   //Default settings: AINP=AIN3, AINN=AVSS, Gain 1, PGA enabled
    m_config_reg1 = 0x00;   //Default settings: DR=20 SPS, Mode=Normal, Conv mode=: Single-shot mode, Temp Sensor disabled, Current Source off
    m_config_reg2 = 0xE0;   //Default settings: Analog supply (AVDD – AVSS) used as reference,  50-Hz rejection only, power open, IDAC off
    m_config_reg3 = 0x00;   //Default settings: IDAC1 disabled, IDAC2 disabled, DRDY pin only

    #ifdef DEBUG_ADS1220
        printf("ADS1220_writeRegister\r\n");
    #endif
    
    ADS1220_writeRegister( CONFIG_REG0_ADDRESS , m_config_reg0);
    ADS1220_writeRegister( CONFIG_REG1_ADDRESS , m_config_reg1);
    ADS1220_writeRegister( CONFIG_REG2_ADDRESS , m_config_reg2);
    ADS1220_writeRegister( CONFIG_REG3_ADDRESS , m_config_reg3);
    
    #ifdef DEBUG_ADS1220
        printf(" Done\r\n");
    #endif

      delay_ms(100);

    #ifdef DEBUG_ADS1220
        printf("ADS1220_readRegister\r\n");
    #endif
    
    Config_Reg0 = ADS1220_readRegister(CONFIG_REG0_ADDRESS);
    Config_Reg1 = ADS1220_readRegister(CONFIG_REG1_ADDRESS);
    Config_Reg2 = ADS1220_readRegister(CONFIG_REG2_ADDRESS);
    Config_Reg3 = ADS1220_readRegister(CONFIG_REG3_ADDRESS);

    #ifdef DEBUG_ADS1220
        printf("Config_Reg : \r\n");
        printf("0x%x \r\n",Config_Reg0);
        printf("0x%x \r\n",Config_Reg1);
        printf("0x%x \r\n",Config_Reg2);
        printf("0x%x \r\n",Config_Reg3);
    #endif
        
    adc_cs=1;

    delay_ms(100);

    //Start_Conv();
    delay_ms(100);
}

void ADS1220_SPI_Command(unsigned char data_in)
{
      adc_cs=0;
      delay_ms(2);
      adc_cs=1;
      delay_ms(2);
      adc_cs=0;
      delay_ms(2);
      spi(data_in);
      delay_ms(2);
      adc_cs=1;
}

void ADS1220_Reset()
{
    ADS1220_SPI_Command(RESET);
}

void ADS1220_Start_Conv()
{
   ADS1220_SPI_Command(START_ADS1220); 
}

void ADS1220_PGA_ON(void)
{
    m_config_reg0 &= ~_BV(0);
    ADS1220_writeRegister(CONFIG_REG0_ADDRESS,m_config_reg0);
}

void ADS1220_PGA_OFF(void)
{
    m_config_reg0 |= _BV(0);
    ADS1220_writeRegister(CONFIG_REG0_ADDRESS,m_config_reg0);
}

void ADS1220_set_conv_mode_continuous(void)
{
    m_config_reg1 |= _BV(2);
    ADS1220_writeRegister(CONFIG_REG1_ADDRESS,m_config_reg1);
}

void ADS1220_set_conv_mode_single_shot(void)
{
    m_config_reg1 &= ~_BV(2);
    ADS1220_writeRegister(CONFIG_REG1_ADDRESS,m_config_reg1);
}

void ADS1220_set_data_rate(int datarate)
{
    m_config_reg1 &= ~REG_CONFIG1_DR_MASK;
    m_config_reg1 |= datarate;
    ADS1220_writeRegister(CONFIG_REG1_ADDRESS,m_config_reg1);
}

void ADS1220_select_mux_channels(int channels_conf)
{
    m_config_reg0 &= ~REG_CONFIG0_MUX_MASK;
    m_config_reg0 |= channels_conf;
    ADS1220_writeRegister(CONFIG_REG0_ADDRESS,m_config_reg0);
}

void ADS1220_set_pga_gain(int pgagain)
{
    m_config_reg0 &= ~REG_CONFIG0_PGA_GAIN_MASK;
    m_config_reg0 |= pgagain ;
    ADS1220_writeRegister(CONFIG_REG0_ADDRESS,m_config_reg0);
}

uint8_t * ADS1220_get_config_reg()
{
    static uint8_t config_Buff[4];

    m_config_reg0 = ADS1220_readRegister(CONFIG_REG0_ADDRESS);
    m_config_reg1 = ADS1220_readRegister(CONFIG_REG1_ADDRESS);
    m_config_reg2 = ADS1220_readRegister(CONFIG_REG2_ADDRESS);
    m_config_reg3 = ADS1220_readRegister(CONFIG_REG3_ADDRESS);

    config_Buff[0] = m_config_reg0 ;
    config_Buff[1] = m_config_reg1 ;
    config_Buff[2] = m_config_reg2 ;
    config_Buff[3] = m_config_reg3 ;

    return config_Buff;
}

int32_t ADS1220_Read_WaitForData()
{
      static uint8_t SPI_Buff[3];
      int32_t mResult32=0;
      long int bit24;
      int i;     
      
      while(adc_drdy);             //        Wait for DRDY to transition low

      adc_cs = 0;                          //Take CS low
      delay_us(100);
      for (i = 0; i < 3; i++)
      {
            SPI_Buff[i] = spi(SPI_MASTER_DUMMY);
      }
      delay_us(100);
      adc_cs = 1;                   //  Clear CS to high

      bit24 = SPI_Buff[0];
      bit24 = (bit24 << 8) | SPI_Buff[1];
      bit24 = (bit24 << 8) | SPI_Buff[2];                                 // Converting 3 bytes to a 24 bit int

      bit24= ( bit24 << 8 );
      mResult32 = ( bit24 >> 8 );                      // Converting 24 bit two's complement to 32 bit two's complement

      return mResult32;
}

int32_t ADS1220_Read_SingleShot_WaitForData(void)
{
    static uint8_t SPI_Buff[3];
    int32_t mResult32=0;
    long int bit24;
    int i;     
      
    ADS1220_Start_Conv();

    adc_cs = 0;                         //Take CS low
    delay_us(100);
                        
    #ifdef DEBUG_ADS1220
        printf("WAIT DRDY\r\n");
    #endif

    while(adc_drdy); //        Wait for DRDY to transition low

    #ifdef DEBUG_ADS1220
        printf(" DETECT\r\n");
    #endif

    for (i = 0; i < 3; i++)
    {
      SPI_Buff[i] = spi(SPI_MASTER_DUMMY);
    }
    delay_us(100);
    adc_cs = 1;                  //  Clear CS to high

    bit24 = SPI_Buff[0];
    bit24 = (bit24 << 8) | SPI_Buff[1];
    bit24 = (bit24 << 8) | SPI_Buff[2];                                 // Converting 3 bytes to a 24 bit int

    bit24= ( bit24 << 8 );
    mResult32 = ( bit24 >> 8 );                      // Converting 24 bit two's complement to 32 bit two's complement
    
    #ifdef DEBUG_ADS1220
        printf("Result 0x%x\r\n",mResult32);
    #endif
    
    return mResult32;
}

int32_t ADS1220_Read_SingleShot_SingleEnded_WaitForData(uint8_t channel_no)
{
      static uint8_t SPI_Buff[3];
      int32_t mResult32=0;
      long int bit24;
      int i;
         
      adc_cs = 0;                         //Take CS low
      delay_us(100);
      
      ADS1220_select_mux_channels(channel_no);
      delay_ms(100);

      ADS1220_Start_Conv();
      delay_ms(100);

      while(adc_drdy);    //        Wait for DRDY to transition low

      for (i = 0; i < 3; i++)
      {
      SPI_Buff[i] = spi(SPI_MASTER_DUMMY);
      }
      delay_us(100);
      adc_cs = 1;                  //  Clear CS to high

      bit24 = SPI_Buff[0];
      bit24 = (bit24 << 8) | SPI_Buff[1];
      bit24 = (bit24 << 8) | SPI_Buff[2];                                 // Converting 3 bytes to a 24 bit int

      bit24= ( bit24 << 8 );
      mResult32 = ( bit24 >> 8 );                      // Converting 24 bit two's complement to 32 bit two's complement

    return mResult32;
}

float convertToMilliV(int32_t i32data)
{
    return (float)((i32data*VFSR*1000)/FULL_SCALE);
}